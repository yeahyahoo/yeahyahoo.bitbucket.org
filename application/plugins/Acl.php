<?php

class Plugin_Acl extends Zend_Controller_Plugin_Abstract {
    /*
     *  This plugin is for checking user's role before processing request
     *  so that users can only use certain actions
     *  This class is used with Model_Acl class
     */

    public function preDispatch(Zend_Controller_Request_Abstract $request) {

        $acl = $acl = Zend_Registry::get("acl");
        $resource = $request->getControllerName();
        $action = $request->getActionName();
        
        $auth = Zend_Auth::getInstance();
        
        if ($auth->hasIdentity()) {
            $user = $auth->getIdentity();
            
            $user_role = $user['role'];
        }else{
            $user_role = "guest";
        }
        
        if (!$acl->isAllowed($user_role, $resource, $action)) {
            $request->setControllerName("error");
            $request->setActionName("error");
        }
    }
}

?>