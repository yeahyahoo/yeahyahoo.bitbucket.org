<?php

/*
 *  Including all the roles, privileges that users can access
 */

class Model_Acl {

    public $acl;

    public function __construct() {
        $this->acl = new Zend_Acl();
    }

    public function setRoles() {
        /// There are four roles: guest, reader, writer, admin
        $this->acl->addRole(new Zend_Acl_Role("guest"));
        $this->acl->addRole(new Zend_Acl_Role("user"), "guest");
        $this->acl->addRole(new Zend_Acl_Role("admin"), "user");
    }

    public function setResources() {
        $this->acl->add(new Zend_Acl_Resource("index"));
        $this->acl->add(new Zend_Acl_Resource("admin"));
        $this->acl->add(new Zend_Acl_Resource("program"));
        $this->acl->add(new Zend_Acl_Resource("be"));
        $this->acl->add(new Zend_Acl_Resource("user"));
        $this->acl->add(new Zend_Acl_Resource("classifier"));
        $this->acl->add(new Zend_Acl_Resource("dataset"));
        $this->acl->add(new Zend_Acl_Resource("error"));
        $this->acl->add(new Zend_Acl_Resource("process"));
        $this->acl->add(new Zend_Acl_Resource("compiler"));
    }

    public function setPrivileges() {

        /*
         * map a role to a controller action.
         */
        /// Example: guest can use 'comment' controller , action 'add'
        $this->acl->allow("guest", "index", array("index", "logout", "construction"));

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/settings.ini', 'main');
        $signup = $config->settings->signup;

        if($signup=="true"){
            $this->acl->allow("guest", "user", array("login", "checkstatus", "signup"));
        }else{
            $this->acl->allow("guest", "user",array("login","checkstatus"));
        }

        $this->acl->allow("guest", "error", array("error"));
        $this->acl->allow("guest", "be", array("index", "login"));
        $this->acl->deny("guest");

        $this->acl->allow("user", "index", array("upload"));
        $this->acl->allow("user", "user", "changepassword");

        $this->acl->allow("user", "program", array("getallnames", "getall", "newtraining", "continuetraining", "rundatasetprogram"));

        $this->acl->allow("user", "classifier", array("getallnames", "getbyid", "savename", "deletebyid", "deleteprocess", "getclassifierprocesses", "preparetodraw"));

        $this->acl->allow("user", "dataset", array("getallnames", "getbyid", "savename", "deletebyid", "merge", "duplicate", "preparetodraw", "download"));

        $this->acl->allow("user", "process", array("getactive", "getpage", "stopprocess"));

        $this->acl->allow("admin");
    }

    public function setAcl() {
        /// set the instance of Zend_Acl to registry.
        Zend_Registry::set("acl", $this->acl);
    }
}

?>