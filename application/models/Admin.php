<?php
/**
 *
 */
class Model_Admin extends MongoCollection {
	
	public function __construct() {
		$conn = new Mongo("localhost");
		$db = $conn -> selectDB("spitz");
		parent::__construct($db, "admin");
	}

	public function find($crit = array(),$fields = array()) {
		return parent::find($crit,$fields);
	}

	public function findById($id) {
		return parent::findOne(array('_id' => new MongoId($id)));
	}

	public function insert($a, array $options = array()){
		try{
			parent::insert($a, $options);
			return true;
		}
		catch(MongoCursorException $e){
			return false;
		}
	}

	public function delete($id) {
		try {
			parent::remove(array('_id' => new MongoId($id)));
			return true;
		} catch(MongoCursorException $e) {
			return false;
		}
	}
	
	public function update($crit, $newObj, array $options = NULL){
		return parent::update($crit, $newObj);
	}

}
?>