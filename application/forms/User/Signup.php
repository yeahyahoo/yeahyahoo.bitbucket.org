<?php

class Form_User_Signup extends Zend_Form {

    public function init() {
        $email_length = new Zend_Validate_StringLength(
                array('min' => 3, 'max' => 50)
        );

        $this->addElement("text", "email", array("label" => "Email", "description" =>
            "Bewteen 3 to 50 characters", "required" => true,
            'filters' => array('StringTrim', 'StringToLower'),
            "validators" => array("EmailAddress", $email_length)
                )
        );

        $password_length = new Zend_Validate_StringLength(
                array('min' => 3, 'max' => 50)
        );

        $this->addElement("password", "password", array("label" => "Password", "required" => true,
            "validators" => array($password_length)
                )
        );
    }
}