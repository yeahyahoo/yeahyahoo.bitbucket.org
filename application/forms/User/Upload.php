<?php

class Form_User_Upload extends Zend_Form {

    public function init() {
        $this->setAttrib('id', 'upload-form');
        $this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
        
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/settings.ini', 'main');
        $MAX_DATA_SIZE = intval($config->settings->max_dataset_size)*1000;
        
        $dataset_file = new Zend_Form_Element_File('csv_file');
        $dataset_file->setLabel("File:");
        $dataset_file->setDescription('(less than 300Kb)');
        $dataset_file->setRequired(true)->setMaxFileSize($MAX_DATA_SIZE); // limits the filesize on the client side
        $dataset_file->addValidator('Count', false, 1);
        
        $dataset_file->addValidator('Size', false, $MAX_DATA_SIZE);            //
        $dataset_file->addValidator('Extension', false, array('csv','txt')); // 
        
        $name = new Zend_Form_Element_Text('name');
        $name->setLabel("Name:");
        $name->setAttrib('class', 'form-control');
        $name->setRequired(true);
        
        $csv_separation = new Zend_Form_Element_Checkbox('csv_seperation');
        $csv_separation->setLabel('Convert from space to comma separation');
        $csv_separation->setDecorators(array('ViewHelper',
            'Errors',
            array('Label',
                array('placement' => 'APPEND', 'style' => 'margin-left:4px; margin-top:15px')),
            array('HtmlTag', array('tag' => 'div'))));
        
                
        $this->addElement($dataset_file);
        $this->addElement($name);
        $this->addElement($csv_separation);
        $this->addElement("submit", "upload-btn", array("label" => "Upload", "class" => "btn btn-primary"));
    }

}