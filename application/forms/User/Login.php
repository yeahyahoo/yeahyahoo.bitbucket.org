<?php

class Form_User_Login extends Zend_Form {

    public function init() {
        $email_length = new Zend_Validate_StringLength(
                array('min' => 3, 'max' => 50)
        );

        $this->addElement("text", "email", array(
            "label" => "Email",
            "required" => true,
            'filters' => array('StringTrim', 'StringToLower'),
            'validators' => array(
                'EmailAddress',
                $email_length
            ),
        ));

        $pass_length = new Zend_Validate_StringLength(
                array('min' => 3, 'max' => 50)
        );

        $this->addElement("password", "password", array(
            "label" => "Password",
            "required" => true,
            'validators' => array(
                $pass_length
            ),
        ));
    }

}
