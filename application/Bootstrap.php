<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    protected function _initAutoLoader() {
        
        $_SERVER["REQUEST_URI"] = str_replace('index.php', '', $_SERVER["REQUEST_URI"]); /// hacky code, remove /index.php from request url
        
        $modelLoader = new Zend_Application_Module_Autoloader(array('namespace' => '', 'basePath' => APPLICATION_PATH));
        $this->bootstrap('frontcontroller');
        $controller = Zend_Controller_Front::getInstance();
        $controller->setBaseUrl('/~tlam/');
        $controller->throwExceptions(true);
        
        /*
         *  Instantiate Zend access control list (ACL)
         */
        $acl = new Model_Acl();
        $acl->setRoles();
        $acl->setResources();
        $acl->setPrivileges();
        $acl->setAcl();
        
        // and install Plugin Acl
        $fc = Zend_Controller_Front::getInstance();
        $fc->registerPlugin(new Plugin_Acl());

        return $modelLoader;
    }

    protected function _initViewSetting() {

        date_default_timezone_set('America/Los_Angeles');

        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->setEncoding('UTF-8');
        $view->doctype('HTML5');
        $view->headMeta()->appendHttpEquiv('Content-Type', 'text/html; charset=UTF-8');
        $view->headMeta()->appendHttpEquiv('Content-Language', 'en-US');
    }
}