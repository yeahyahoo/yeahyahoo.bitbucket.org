<?php

class CompilerController extends Zend_Controller_Action {

    public function init() {
        //$this->_helper->viewRenderer->setNoRender(true);
    }

    public function createfolderAction() {
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $folder_model = new Model_Folders();
            $folder = array();
            $folder['_id'] = new MongoId();
            $full_path = APPLICATION_PATH . "/.." . "/programs/" . $formData['type'] . "/" . $folder['_id'];

            if (!file_exists($full_path) && !is_dir($full_path)) {
                
                if (mkdir($full_path)) {
                    chmod($full_path, 0700);
                    
                    //// insert
                    $folder['type'] = $formData['type'];
                    $folder['name'] = $formData['name'];
                    $folder['index'] = intval($formData['index']);
                    $folder['programs'] = array();
                    $folder['show'] = false;
                    $folder_model->insert($folder);
                    $folder['id'] = utf8_encode($folder['_id']);
                    
                    $this->getHelper('json')->sendJson(array(
                        'message' => "The folder " . $formData['name'] . " is created successfully.",
                        'folder' => $folder,
                        'result' => true
                    ));
                }
            }

            $this->getHelper('json')->sendJson(array(
                'message' => "Can't create the folder.",
                'result' => false
            ));
        }
    }

    public function renamefolderAction() {
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $folder_model = new Model_Folders();
            $folder_model->update(array('_id' => new MongoId($formData['id'])), array('$set' => array("name" => $formData['new_name'])));

            $this->getHelper('json')->sendJson(array(
                'message' => "The folder name has been changed.",
                'result' => true
            ));
        }
    }

    public function deletefolderAction() {
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $folder_model = new Model_Folders();
            $folder = $folder_model->findById($formData['id']);

            if (!$folder) {
                $this->getHelper('json')->sendJson(array(
                    'message' => "Can't delete the folder.",
                    'result' => false
                ));
            }
            $full_path = APPLICATION_PATH . "/.." . "/programs/" . $folder['type'] . "/" . $formData['id'];

            if (file_exists($full_path) && is_dir($full_path)) {

                $files = @scandir($full_path);
                if (count($files) > 2) {
                    $this->getHelper('json')->sendJson(array(
                        'message' => "There are programs in the folder. Please delete them first.",
                        'result' => false
                    ));
                } else if (rmdir($full_path)) {
                    $folder_model->delete($formData['id']);
                    /// reoder folders
                    $folder_model = new Model_Folders();
                    $folder_cursor = $folder_model->find()->sort(array('index'=>1));
                    
                    $i = 0;
                    foreach ($folder_cursor as $key => $val) {
                        if ($val['type'] == $folder['type']) {
                            $folder_model->update(array('_id' => $val['_id']), array('$set' => array("index" => $i)));
                            ++$i;
                        }
                    }
                    
                    $this->getHelper('json')->sendJson(array(
                        'message' => "The folder " . $folder['name'] . " is deleted successfully.",
                        'result' => true
                    ));
                }
            }

            $this->getHelper('json')->sendJson(array(
                'message' => "Can't delete the folder.",
                'result' => false
            ));
        }
    }

    public function folderupAction(){
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $folder_model = new Model_Folders();
            $index = intval($formData['index']);
            $indexUp = $index - 1;
            $folder_model->update(array('_id' => new MongoId($formData['folderUp'])), array('$set' => array("index" => $indexUp)));
            $folder_model->update(array('_id' => new MongoId($formData['folderDown'])), array('$set' => array("index" => $index)));
        }
    }
    
    public function folderdownAction(){
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $folder_model = new Model_Folders();
            $index = intval($formData['index']);
            $indexDown = $index + 1;
            $folder_model->update(array('_id' => new MongoId($formData['folderUp'])), array('$set' => array("index" => $index)));
            $folder_model->update(array('_id' => new MongoId($formData['folderDown'])), array('$set' => array("index" => $indexDown)));
        }
    }
    
    public function foldervisibilityAction() {
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $folder_model = new Model_Folders();
            $folder_model->update(array('_id' => new MongoId($formData['id'])), array('$set' => array("show" => $formData['show'])));
            
            $this->getHelper('json')->sendJson(array(
                'result' => true
            ));
        }
        
        $this->getHelper('json')->sendJson(array(
            'result' => false
        ));
    }
    
    public function newAction() {
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost(); // name, language, 
            
            $program = array();
            $program['_id'] = new MongoId();
            
            $extension = '.c';
            if ($formData['language'] == 'python') {
                $extension = '.py';
            }
            
            $full_path = $this->getFullpath($formData['type'], $formData['folder'], $program['_id'], $formData['language']);
            /// check if the file exists, unlikely to happen
            if (file_exists($full_path)) {
                $this->getHelper('json')->sendJson(array(
                    'message' => "The program already exists.",
                    'result' => false
                ));
            }

            //// create a file with template code
            if (($fileHandler = fopen($full_path, 'w')) == null) {
                $this->getHelper('json')->sendJson(array(
                    'message' => "Can't create the program.",
                    'result' => false
                ));
            }

            /// write 
            if($formData['language']=='c'){
                $template_file = APPLICATION_PATH . "/.." ."/programs/c_code.tmpl";
            }else{
                $template_file = APPLICATION_PATH . "/.." ."/programs/python_code.tmpl";
            }
            
            $content = file_get_contents($template_file);
            fwrite($fileHandler, $content);
            fclose($fileHandler);
            
            /// set permission 
            chmod($full_path, 0700);
            
            /// adding the program to algorithm database
            $program_model = new Model_Programs();

            $program['type'] = $formData['type'];
            $program['name'] = $formData['name'];
            $program['folder'] = $formData['folder'];
            $program['language'] = $formData['language'];
            $program['enable'] = 0;
            $program['theme'] = 'default';
            $program['description'] = '';
            $program['arguments'] = array();

            $program_model->insert($program);
            $program['id'] = utf8_encode($program['_id']);
            
            $this->getHelper('json')->sendJson(array(
                'message' => "A new program has been created.",
                'result' => true,
                'program' => $program,
                'content' => $content
            ));
        }
    }

    public function openAction() {
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost(); // id

            $program_model = new Model_Programs();
            $program = $program_model->findById($formData['id']);
            $program['id'] = utf8_encode($program['_id']);

            if (!$program) {
                $this->getHelper('json')->sendJson(array(
                    'message' => "Can't find program.",
                    'result' => false
                ));
            }

            $full_path = $this->getFullpath($program['type'],$program['folder'], $program['id'], $program['language']);

            if (!file_exists($full_path)) {
                $this->getHelper('json')->sendJson(array(
                    'message' => "The program doesn't exist.",
                    'result' => false
                ));
            }

            $file = file_get_contents($full_path);

            $this->getHelper('json')->sendJson(array(
                'message' => "The program " . $program['name'] . " has been opened.",
                'content' => $file,
                'program' => $program,
                'result' => true
            ));
        }
    }

    public function saveAction() {
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost(); // id, content

            $program_model = new Model_Programs();
            $program = $program_model->findById($formData['program']['id']);

            if (!$program) {
                $this->getHelper('json')->sendJson(array(
                    'message' => "Can't find program.",
                    'result' => false
                ));
            }

            $full_path = $this->getFullpath($program['type'],$program['folder'], $formData['program']['id'], $program['language']);

            if (!file_exists($full_path)) {
                $this->getHelper('json')->sendJson(array(
                    'message' => "The file doesn't exist.",
                    'result' => false
                ));
            }

            file_put_contents($full_path, $formData['content']);
            
            /// save to database
            if(!isset($formData['program']['arguments'])){
                $formData['program']['arguments'] = array();
            }
            
            $no_value_argments = array();
            foreach ($formData['program']['arguments'] as $arg) {
                $no_value_argments[] = array('name' => $arg['name'], 'hint' => $arg['hint']);
            }

            $program_model->update(array('_id' => new MongoId($formData['program']['id'])), array('$set' => array("arguments" => $no_value_argments, "description" => $formData['program']['description'])));

            $this->getHelper('json')->sendJson(array(
                'message' => 'The program has been saved.',
                'result' => true
            ));
        }
    }

    public function compileAction() {
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost(); // id, content

            $program_model = new Model_Programs();
            $program = $program_model->findById($formData['program']['id']);
            $program['id'] = utf8_encode($program['_id']);
            
            if (!$program) {
                $this->getHelper('json')->sendJson(array(
                    'message' => "Can't find program.",
                    'result' => false
                ));
            }
            
            $folder_model = new Model_Folders();
            $folder = $folder_model->findById($program['folder']);
            $folder['id'] = utf8_encode($folder['_id']);
            
            $full_path = $this->getFullpath($program['type'], $program['folder'], $formData['program']['id'], $program['language']);
            if (!file_exists($full_path)) {
                $this->getHelper('json')->sendJson(array(
                    'message' => "The file doesn't exist.",
                    'result' => false
                ));
            }
            
            /// write content to file
            file_put_contents($full_path, $formData['content']);
            /// save to database
            if(!isset($formData['program']['arguments'])){
                $formData['program']['arguments'] = array();
            }
            
            $no_value_argments = array();
            foreach ($formData['program']['arguments'] as $arg) {
                $no_value_argments[] = array('name' => $arg['name'], 'hint' => $arg['hint']);
            }

            $program_model->update(array('_id' => new MongoId($formData['program']['id'])), array('$set' => array("arguments" => $no_value_argments, "description" => $formData['program']['description'])));

            //// compile it,
            $output = array(); ///
            $compiling_path = "../programs/" . $program['type'] . "/" . $folder['id'] . "/";

            if ($program['language'] == 'c') {
                $commandline = "gcc -o " . $compiling_path . $program['id'] . ".out " . $compiling_path . $program['id'] . ".c -ljansson -llbfgs -lm 2>&1";
            }
            
            $terminalStatus = 0;
            exec($commandline, $output, $terminalStatus);
            
            $this->getHelper('json')->sendJson(array(
                'message' => 'The program is compiled.',
                'terminal' => $output,
                'terminalStatus' => intval($terminalStatus),
                'result' => true
            ));
        }
    }

    public function renameAction() {
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost(); // id, new name

            $program_model = new Model_Programs();
            $program_model->update(array('_id' => new MongoId($formData['id'])), array('$set' => array("name" => $formData['name'])));

            $this->getHelper('json')->sendJson(array(
                'message' => "The program name has been changed.",
                'result' => true
            ));
        }
    }

    public function deleteAction() {
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost(); // id

            $full_path = $this->getFullpath($formData['type'],$formData['folder'], $formData['id'], $formData['language']);

            if (!file_exists($full_path)) {
                $this->getHelper('json')->sendJson(array(
                    'message' => "The file doesn't exist.",
                    'result' => false
                ));
            }

            unlink($full_path);

            $program_model = new Model_Programs();
            $program_model->delete($formData['id']);
            
            $this->getHelper('json')->sendJson(array(
                'message' => 'Program ' . $formData['name'] . ' is deleted.',
                'result' => true
            ));
        }
    }

    public function updatethemeAction() {
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost(); // id, theme
            $admin_model = new Model_Admin();

            $auth = Zend_Auth::getInstance();
            $admin = null;
            if ($auth->hasIdentity()) {
                $admin = $auth->getIdentity();
            } else {
                $this->getHelper('json')->sendJson(array(
                    'message' => "Error",
                    'result' => false
                ));
            }

            $admin_model->update(array('_id' => new MongoId($admin['id'])), array('$set' => array("theme" => $formData['theme'])));

            $this->getHelper('json')->sendJson(array(
                'message' => "Theme changes successfully.",
                'result' => true
            ));
        }
    }

    public function loadthemeAction() {
        $admin_model = new Model_Admin();

        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $admin = $auth->getIdentity();
        } else {
            $this->getHelper('json')->sendJson(array(
                'message' => "Error",
                'result' => false
            ));
        }

        $admin = $admin_model->findById($admin['id']);

        $this->getHelper('json')->sendJson(array(
            'theme' => $admin['theme'],
            'result' => true,
        ));
    }

    public function getFullpath($type, $folder_id, $program_id, $language) {

        $extension = '.c';
        if ($language == 'python') {
            $extension = '.py';
        }

        $full_path = APPLICATION_PATH . "/.." . "/programs/" . $type . "/" . $folder_id . "/" . $program_id . $extension;
        return $full_path;
    }

    public function getdatasetsAction() {

        $dataset_model = new Model_Datasets();
        $dataset_cursor = $dataset_model->find(array('processing' => false));

        $datasets = array();
        $d = 0;
        foreach ($dataset_cursor as $dataset) {
            $dataset['id'] = utf8_encode($dataset['_id']);
            $dataset['key'] = $d;
            $dataset['path'] = $dataset['user_id'] . "/datasets/" . $dataset['id'] . ".csv";
            $datasets[] = $dataset;
            $d++;
        }

        $this->getHelper('json')->sendJson($datasets);
    }

    public function runAction() {
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $dataset_id = $formData['dataset_id'];
            
            $auth = Zend_Auth::getInstance();
            if ($auth->hasIdentity()) {
                $admin = $auth->getIdentity();
            } else {
                $this->getHelper('json')->sendJson(array('result' => false));
            }
            
            $dataset_model = new Model_Datasets();
            $dataset = $dataset_model->findById($dataset_id);
            $dataset_model->update(array('_id'=>new MongoId($dataset_id)), array('$set' => array(
                "processing" => true,
            )));
            
            $program_model = new Model_Programs();
            $program = $program_model->findById($formData['program']['id']);
            $program['id'] = utf8_encode($program['_id']);

            if (!$program) {
                $this->getHelper('json')->sendJson(array(
                    'message' => "Can't find program.",
                    'result' => false
                ));
            }

            $folder_model = new Model_Folders();
            $folder = $folder_model->findById($program['folder']);
            $folder['id'] = utf8_encode($folder['_id']);

            /// save program first, should be similar to save and compile action
            $full_path = $this->getFullpath($program['type'], $program['folder'], $formData['program']['id'], $program['language']);

            if (!file_exists($full_path)) {
                $this->getHelper('json')->sendJson(array(
                    'message' => "The file doesn't exist.",
                    'result' => false
                ));
            }

            file_put_contents($full_path, $formData['content']);
            /// save to database
            if (!isset($formData['args'])) {
                $formData['args'] = array();
            }

            $no_value_argments = array();
            foreach ($formData['args'] as $arg) {
                $no_value_argments[] = array('name' => $arg['name'], 'hint' => $arg['hint']);
            }

            $program_model->update(array('_id' => new MongoId($formData['program']['id'])), array('$set' => array("arguments" => $no_value_argments, "description" => $formData['program']['description'])));

            $user_output_path = APPLICATION_PATH . "/.." . '/admin_data/'.$admin['id']."/";

            /// Prepare arguments for program
            $extension = $program['language'] == 'c' ? '.out' : '.py'; /// c => out
            $program_full_path = APPLICATION_PATH . "/.." . "/programs/" . $program['type'] . "/" . $folder['id'] . "/" . $program['id'] . $extension;
            $dataset_full_path = APPLICATION_PATH . "/.." . "/user_data/" . $formData['data_path'];
            
            if (!file_exists($program_full_path) || !file_exists($dataset_full_path)) {
                $this->getHelper('json')->sendJson(array(
                    'message' => "Files don't exist." . $program_full_path,
                    'result' => false
                ));
            }

            /// Here 'output' is the id, b/c for admin,
            /// we don't need to store 1 output at a time.
            /// If we are in front-end, 'output' would be an hashed number

            $classifier_id = 'classifier_id'; /// since we try a test run, we don't have a classifier id

            if ($program['type'] == 'classifier') {
                $arguments = escapeshellarg($user_output_path) . " " . $admin['id'] . " " . $classifier_id . " " . $formData['run_type'] . " " . escapeshellarg($dataset_full_path);
            } else {
                $arguments = escapeshellarg($user_output_path) . " " . $admin['id'] . " " . escapeshellarg($dataset_full_path);
            }

            foreach ($formData['args'] as $argument) {
                $arguments .= " " . escapeshellarg($argument['value']);
            }

            if ($program['language'] == 'c') {
                $commandline = $program_full_path . " " . $arguments . " 2>&1";
            } else {
                $commandline = "python " . $program_full_path . " " . $arguments . " 2>&1";
            }

            $job_file_path = APPLICATION_PATH . "/.." . "/job_engine/admin_job.php";
            $script_dir = "php -c /etc/php5/apache2/php.ini " . $job_file_path;
            $command = $script_dir . " \"" . $commandline . "\" " . $admin['id'] . " " . $dataset_id;
            
            $pid = exec("nohup $command > /dev/null 2>&1 & echo $!");

            $admin_model = new Model_Admin();
            $admin_model->update(array("_id" => new MongoId($admin['id'])), array('$set' => array(
                    "pid" => $pid,
                    "program_status" => "active",
                    "program_output" => "",
                    "program_name" => $program['name'],
                    "dataset_name" => $dataset['name'],
                    "arguments" => $formData['args'],
                    "dataset_id" => $dataset_id
            )));
            
            $runningProgram = array();
            $runningProgram['program_name'] = $program['name'];
            $runningProgram['dataset_name'] = $dataset['name'];
            $runningProgram['arguments'] = $formData['args'];
            
            $this->getHelper('json')->sendJson(array(
                "program" => $runningProgram,
                "result" => true
            ));
        }
        $this->getHelper('json')->sendJson(array("result" => false));
    }
    
    public function stopprocessAction(){
        if ($this->getRequest()->isPost()) {
            /// Check for authentication
            $auth = Zend_Auth::getInstance();
            if ($auth->hasIdentity()) {
                $admin = $auth->getIdentity();
            } else {
                $this->getHelper('json')->sendJson(array("result" => false));
            }

            $admin_model = new Model_Admin();
            $admin = $admin_model->findById($admin['id']);
            $admin['id'] = utf8_encode($admin['_id']);
            
            $pid = intval($admin['pid']);
            
            if ($admin['program_status'] == 'active' && $pid>0) {
                exec("kill $pid");
                
                $admin_output_path = APPLICATION_PATH . "/.." . "/admin_data/" . $admin['id']."/";
                /*
                $all_files = glob($admin_output_path.'*');
                foreach ($all_files as $file) {
                    if (is_file($file)){
                        unlink($file);
                    }
                }
                */
                //// set dataset to not being processed
                $dataset_model = new Model_Datasets();
                $dataset_model->update(array("_id" => new MongoId($admin['dataset_id'])), array('$set' => 
                    array('processing' => false)
                ));
                
                $admin_model->update(array("_id" => new MongoId($admin['id'])), array('$set' => array(
                        "pid" => null,
                        "program_status" => "done",
                        "program_output" => "",
                        "dataset_id" => null
                )));
            }
            $this->getHelper('json')->sendJson(array("result" => true));
        }
        
        $this->getHelper('json')->sendJson(array(
            "result" => false
        ));
    }
    
    public function checkprogramstatusAction(){
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $admin = $auth->getIdentity();
        } else {
            $this->getHelper('json')->sendJson(array("result" => false));
        }

        $admin_model = new Model_Admin();
        $admin = $admin_model->findById($admin['id']);
        $admin['id'] = utf8_encode($admin['_id']);
        
        $program = array();
        $program['program_name'] = $admin['program_name'];
        $program['dataset_name'] = $admin['dataset_name'];
        $program['arguments'] = $admin['arguments'];
        
        if($admin['program_status']=='active'){
            
            $this->getHelper('json')->sendJson(array(
                "runningProgram" => $program,
                "result" => true
            ));
        }else{
            $admin_output_path = APPLICATION_PATH . "/.." . '/admin_data/'.$admin['id']."/";
            $json_file_path = $admin_output_path. $admin['id']. '.json';
            
            $json_download = false;
            $json_file = null;
            $decoded_json = null;
            $MAX_JSON_FILE = 100000000; /// 100 MB, replace this with const later

            if (file_exists($json_file_path)) {
                if (filesize($json_file_path) < $MAX_JSON_FILE) {
                    $json_file = file_get_contents($json_file_path);
                    $decoded_json = json_decode($json_file);
                } else {
                    $json_download = true;
                }
            }

            $images = array();
            if (!$json_download && $decoded_json) {
                foreach ($decoded_json as $output) {
                    if (isset($output->type) && $output->type == 'image_plot') {
                        $image_file = base64_encode(file_get_contents($admin_output_path . $output->data->name));
                        $images[] = "data:image/png;base64," . $image_file;
                    }
                }
            }

            $this->getHelper('json')->sendJson(array(
                'runningProgram' => $program,
                'terminal' => $admin['program_output'],
                'terminalStatus' => $admin['program_status'],
                'json' => $json_file,
                'downloadJson' => $json_download,
                'images' => $images,
                'result' => false
            ));
        }
        $this->getHelper('json')->sendJson(array("result" => false));
    }
    
    public function getterminaloutputAction(){
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $admin = $auth->getIdentity();
        } else {
            $this->getHelper('json')->sendJson(array("result" => false));
        }
        
        $admin_model = new Model_Admin();
        $admin = $admin_model->findById($admin['id']);
        
        $runningProgram = array();
        $runningProgram['program_name'] = $admin['program_name'];
        $runningProgram['dataset_name'] = $admin['dataset_name'];
        $runningProgram['arguments'] = $admin['arguments'];
        
        $this->getHelper('json')->sendJson(array(
            'runningProgram' => $runningProgram,
            'terminal' => $admin['program_output'],
            'terminalStatus' => $admin['program_status'],
        ));
    }
    
    public function getoutputfolderAction() {
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $admin = $auth->getIdentity();
        } else {
            $this->getHelper('json')->sendJson(array("result" => false));
        }

        $admin_output_path = APPLICATION_PATH . "/.." . '/admin_data/' . $admin['id'] . "/";
        $all_files = glob($admin_output_path . '*');
        $outputFiles = array();

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/settings.ini', 'main');
        $MAX_FILE = intval($config->settings->max_dataset_size) * 1000;

        foreach ($all_files as $file) {
            $file_info = pathinfo($file);

            if (is_file($file)) {
                if (!isset($file_info['extension'])) {
                    $extension = '';
                } else {
                    $extension = $file_info['extension'];
                }

                $oversize = false;
                if ($extension == 'png') {
                    $content = "data:image/png;base64," . base64_encode(file_get_contents($file));
                } else {
                    if (filesize($file) < $MAX_FILE) {
                        $content = file_get_contents($file);
                    } else {
                        $oversize = true;
                        $content = '';
                    }
                }

                $outputFiles[] = array(
                    'name' => $file_info['basename'],
                    'format' => $extension,
                    'oversize' => $oversize,
                    'content' => $content
                );
            }
        }

        $this->getHelper('json')->sendJson(array(
            'files' => $outputFiles,
            'result' => true,
            'max_file' => ($MAX_FILE / 1000)
        ));
    }

    public function emptyoutputfolderAction() {
        if ($this->getRequest()->isPost()) {
            $auth = Zend_Auth::getInstance();
            if ($auth->hasIdentity()) {
                $admin = $auth->getIdentity();
            } else {
                $this->getHelper('json')->sendJson(array("result" => false));
            }

            $admin_output_path = APPLICATION_PATH . "/.." . '/admin_data/' . $admin['id'] . "/";
            $all_files = glob($admin_output_path . '*');

            foreach ($all_files as $file) {
                if (is_file($file)) {
                    unlink($file);
                }
            }

            $this->getHelper('json')->sendJson(array(
                'result' => true
            ));
        }
    }

    public function deleteoutputfileAction() {
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $auth = Zend_Auth::getInstance();
            if ($auth->hasIdentity()) {
                $admin = $auth->getIdentity();
            } else {
                $this->getHelper('json')->sendJson(array("result" => false));
            }

            $file_path = APPLICATION_PATH . "/.." . '/admin_data/' . $admin['id'] . "/" . $formData['name'];

            if (is_file($file_path)) {
                unlink($file_path);
                $this->getHelper('json')->sendJson(array("result" => true));
            } else {
                $this->getHelper('json')->sendJson(array("result" => false));
            }
        }
    }

}

?>