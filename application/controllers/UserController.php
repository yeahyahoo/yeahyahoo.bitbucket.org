<?php

class UserController extends Zend_Controller_Action {

    public function init() {
        
    }

    public function indexAction() {
        
    }

    public function loginAction() {
        if ($this->getRequest()->isPost()) {
            $model_users = new Model_Users();
            $authAdapter = new Zend_Auth_Adapter_MongoDb($model_users, 'email', 'password');

            $formData = $this->getRequest()->getPost();
            $email = $this->_request->getPost('email');
            $password = $this->_request->getPost('password');

            $login_form = new Form_User_Login();
            if (!$login_form->isValid($formData)) {
                $this->getHelper('json')->sendJson(array('result' => false));
            }

            $authAdapter->setIdentity($email);
            $authAdapter->setCredential(sha1($password . 'nakamura'));

            $auth = Zend_Auth::getInstance();
            $result = $auth->authenticate($authAdapter);

            if ($result->isValid()) {
                $identity = $authAdapter->getResultDocObject();
                $identity['role'] = 'user';
                $identity['id'] = utf8_encode($identity['_id']);
                unset($identity['_id']);
                unset($identity['password']); //// remove password

                $authStorage = $auth->getStorage();
                $authStorage->write($identity);

                $this->getHelper('json')->sendJson(array('result' => true));
            }
        }
        $this->getHelper('json')->sendJson(array('result' => false));
    }

    public function checkstatusAction() {
        $auth = Zend_Auth::getInstance();

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/settings.ini', 'main');
        $signup = false;
        if ($config->settings->signup == "true") {
            $signup = true;
        }

        if ($auth->hasIdentity()) {
            $identity = $auth->getIdentity();
            $identity['loggedIn'] = true;
            $identity['signUp'] = $signup;
            $this->getHelper('json')->sendJson($identity);
        } else {
            $user = array();
            $user['loggedIn'] = false;
            $user['signUp'] = $signup;
            $this->getHelper('json')->sendJson($user);
        }
    }

    public function signupAction() {
        if ($this->getRequest()->isPost()) {
            $model_users = new Model_Users();
            $formData = $this->getRequest()->getPost();
            
            $signup_form = new Form_User_Signup();
            if (!$signup_form->isValid($formData)) {
                $this->getHelper('json')->sendJson(array('result' => false));
            }

            $existing_user = $model_users->find(array('email' => $formData['email']));

            if ($existing_user->count() > 0) {
                $this->getHelper('json')->sendJson(array("result" => false));
            }

            $newUser = array();
            $newUser['email'] = $formData['email'];
            $password = sha1($formData['password'] . 'nakamura');
            $newUser['password'] = $password;
            $newUser['join_date'] = new MongoDate();

            if ($model_users->insert($newUser)) {
                
                $newUser['id'] = utf8_encode($newUser['_id']);
                $user_dir_path = APPLICATION_PATH . "/../" . "/user_data/" . $newUser['id'];

                if (mkdir($user_dir_path, 0700)) {
                    //// create folders: datasets , out 
                    mkdir($user_dir_path . "/datasets", 0700);
                    mkdir($user_dir_path . "/out", 0700);

                    $authAdapter = new Zend_Auth_Adapter_MongoDb($model_users, "email", "password");
                    $authAdapter->setIdentity($formData['email']);
                    $authAdapter->setCredential($password);

                    $auth = Zend_Auth::getInstance();
                    $result = $auth->authenticate($authAdapter);

                    if ($result->isValid()) {
                        $identity = $authAdapter->getResultDocObject();
                        $identity['role'] = 'user';
                        $identity['id'] = utf8_encode($identity['_id']);
                        unset($identity['_id']);
                        unset($identity['password']); //// remove password

                        $authStorage = $auth->getStorage();
                        $authStorage->write($identity);

                        $this->getHelper('json')->sendJson(array('result' => true));
                    }
                }
            }
        }
        $this->getHelper('json')->sendJson(array('result' => false));
    }

    public function getallusersAction() {

        $user_model = new Model_Users();
        $user_cursor = $user_model->find();

        $users = array();
        foreach ($user_cursor as $user) {
            $user['formated_join_date'] = date('m/d/Y', $user['join_date']->sec);
            $users[] = $user;
        }

        $this->getHelper('json')->sendJson($users);
    }

    public function changepasswordAction() {
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $current_password = trim($formData['current_password']);
            $new_password = trim($formData['new_password']);

            $auth = Zend_Auth::getInstance();

            if ($auth->hasIdentity()) {
                $user = $auth->getIdentity();
            } else {
                $this->getHelper('json')->sendJson(array(
                    'message' => 'You need to log in.',
                    'result' => false
                ));
            }

            $user_model = new Model_Users();
            $user_cursor = $user_model->findById($user['id']);

            $pass_in_db = $user_cursor['password'];

            if ($pass_in_db === sha1($current_password . 'nakamura')) {
                $user_model->update(array("_id" => new MongoId($user['id'])), array('$set' => array(
                        "password" => sha1($new_password . 'nakamura')
                )));

                $this->getHelper('json')->sendJson(array('result' => true));
            } else {
                $this->getHelper('json')->sendJson(array(
                    'message' => 'Current password is not correct.',
                    'result' => false
                ));
            }
        }

        $this->getHelper('json')->sendJson(array(
            'message' => 'Error.',
            'result' => false
        ));
    }

}

?>
