<?php

class ProgramController extends Zend_Controller_Action {

    public function init() {
        
    }

    /*
     *  In process monitor
     */
    public function getbyidAction(){
        
        $formData = $this->getRequest()->getParams();
        $program_model = new Model_Programs();
        $process_model = new Model_Processes();
        $user_model = new Model_Users();
        
        $program = $program_model->findById($formData['id']);
        
        $extension = $program['language'] == 'c' ? '.c' : '.py';
        $program['name'] = $program['name'].$extension;
        
        $process_cursor = $process_model->find(array('program_id'=> $formData['id'], 'status'=>'active'));
        
        $processes = array();
        
        foreach($process_cursor as $process){
            $process['formated_start_time'] = date('M/d/Y - h:i:s', $process['start_time']->sec);
            $sec_diff = $process['end_time']->sec - $process['start_time']->sec;
            $min_str = round($sec_diff/60,1);
            $process['duration'] = $min_str;
            
            $user = $user_model->findByid($process['user_id']);
            $process['user_email'] = $user['email'];
            $processes[] = $process;
        }
        
        $this->getHelper('json')->sendJson(array(
            'program' => $program,
            'processes' => $processes
        ));
    }
    
    public function getallAction() {

        $program_model = new Model_Programs();
        $folder_model = new Model_Folders();
        $folder_cursor = $folder_model->find()->sort(array('index' => 1));

        $classifierFolders = array();
        $datasetFolders = array();

        foreach ($folder_cursor as $folder) {
            $folder['id'] = utf8_encode($folder['_id']);
            $folder['key'] = $folder['index'];
            $program_cursor = $program_model->find(array('folder' => $folder['id']));
            $programs = array();

            $p = 0;
            foreach ($program_cursor as $program) {
                $program['id'] = utf8_encode($program['_id']);
                $program['key'] = $p;
                $program['full_name'] = $program['name'] . '.c';
                if ($program['language'] == 'python') {
                    $program['full_name'] = $program['name'] . '.py';
                }

                $programs[] = $program;
                $p++;
            }

            $folder['programs'] = $programs;

            if ($folder['type'] == 'classifier') {
                $classifierFolders[] = $folder;
            } else {
                $datasetFolders[] = $folder;
            }
        }

        $this->getHelper('json')->sendJson(array(
            'classifierFolders' => $classifierFolders,
            'datasetFolders' => $datasetFolders
        ));
    }

    public function newtrainingAction() {

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            /// get user
            $auth = Zend_Auth::getInstance();
            if ($auth->hasIdentity()) {
                $user = $auth->getIdentity();
            } else {
                $this->getHelper('json')->sendJson(array("result" => false));
            }

            $process_model = new Model_Processes();
            $process_cursor = $process_model->find(array('user_id' => $user['id'], 'status' => 'active'));

            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/settings.ini', 'main');
            $max = intval($config->settings->max_processes);
            if ($process_cursor->count() > $max) {
                $this->getHelper('json')->sendJson(array(
                    'message' => "Reach maximum processes per user ( " . $max . " )",
                    'result' => false
                ));
            }

            // prepare new classifier
            $classifier = array();
            $classifier['_id'] = new MongoId();
            $classifier['program_id'] = $formData['program']['id'];
            $classifier['user_id'] = $user['id'];
            $classifier['name'] = $formData['classifier']['name'];
            $classifier_id = utf8_encode($classifier['_id']);

            /// prepare new process data
            $process = array();
            $process['type'] = 'classifier';
            $process['user_id'] = $user['id'];
            $process['status'] = 'active';
            $process['classifier_id'] = $classifier_id;
            $process['dataset_id'] = $formData['dataset']['id'];
            $process['start_time'] = new MongoDate();
            $process['end_time'] = null;
            $process['program_id'] = $formData['program']['id'];
            $process['output'] = array();

            if(!isset($formData['program']['arguments'])){
                $formData['program']['arguments'] = array();
            }
            
            $input_argments = array();
            foreach ($formData['program']['arguments'] as $arg) {
                $input_argments[] = array('name' => $arg['name'], 'value' => $arg['value']);
            }
            $process['arguments'] = $input_argments;

            $process_model->insert($process);
            $process_id = utf8_encode($process['_id']);

            /// adding the new process to classifier
            $processes = array();
            array_push($processes, $process_id);
            $classifier['processes'] = $processes;

            /// adding classifier
            $classifier_model = new Model_Classifiers();
            if (!$classifier_model->insert($classifier)) {
                $this->getHelper('json')->sendJson(array("result" => false));
            }

            $this->runProgram($classifier_id, $user['id'], $formData['program'], $process['dataset_id'], $process_id, $input_argments, '0');
        }
        $this->getHelper('json')->sendJson(array("result" => false));
    }
   
    public function continuetrainingAction() {

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            /// get user
            $auth = Zend_Auth::getInstance();
            if ($auth->hasIdentity()) {
                $user = $auth->getIdentity();
            } else {
                $this->getHelper('json')->sendJson(array("result" => false));
            }

            $process_model = new Model_Processes();
            $process_cursor = $process_model->find(array('user_id' => $user['id'], 'status' => 'active'));

            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/settings.ini', 'main');
            $max = intval($config->settings->max_processes);
            if ($process_cursor->count() > $max) {
                $this->getHelper('json')->sendJson(array(
                    'message' => "Reach maximum processes per user ( " . $max . " )",
                    'result' => false
                ));
            }

            /// prepare new process data
            $process = array();
            $process['type'] = 'classifier';
            $process['user_id'] = $user['id'];
            $process['status'] = 'active';
            $process['classifier_id'] = $formData['classifier']['id'];
            $process['dataset_id'] = $formData['dataset']['id'];
            $process['start_time'] = new MongoDate();
            $process['end_time'] = null;
            $process['program_id'] = $formData['program']['id'];
            $process['output'] = array();

            if (!isset($formData['program']['arguments'])) {
                $formData['program']['arguments'] = array();
            }

            $input_argments = array();
            foreach ($formData['program']['arguments'] as $arg) {
                $input_argments[] = array('name' => $arg['name'], 'value' => $arg['value']);
            }
            $process['arguments'] = $input_argments;

            $process_model->insert($process);
            $process_id = utf8_encode($process['_id']);

            /// update classifier, add new process.
            $classifier_model = new Model_Classifiers();
            $push = array('$push' => array("processes" => $process_id));
            $classifier_model->update(array("_id" => new MongoId($formData['classifier']['id'])), $push);

            $this->runProgram($formData['classifier']['id'], $user['id'], $formData['program'], $process['dataset_id'], $process_id, $input_argments, '1');
        }
        $this->getHelper('json')->sendJson(array("result" => false));
    }

    public function rundatasetprogramAction() {

        //// create a process
        // status = 'pending', process_id = null, type , program_id , start, end, duration, name, arguments,
        if ($this->getRequest()->isPost()) {
            $program = $this->getRequest()->getPost('program');
            $dataset_id = $this->getRequest()->getPost('dataset_id');

            $process_model = new Model_Processes();
            $dataset_model = new Model_Datasets();
            $folder_model = new Model_Folders();

            /// get dataset, folder
            $dataset = $dataset_model->findById($dataset_id);
            $folder = $folder_model->findById($program['folder']);
            $folder['id'] = utf8_encode($folder['_id']);

            if (!$folder || !$dataset) {
                $this->getHelper('json')->sendJson(array(
                    "message" => "Can't run the program",
                    "result" => false
                ));
            }

            /// receive user
            $auth = Zend_Auth::getInstance();
            if ($auth->hasIdentity()) {
                $user = $auth->getIdentity();
            } else {
                $this->getHelper('json')->sendJson(array(
                    "message" => "Can't run the program",
                    "result" => false
                ));
            }

            /// check number of current dataset processes
            $process_cursor = $process_model->find(array('user_id' => $user['id'], 'status' => 'active'));

            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/settings.ini', 'main');
            $max = intval($config->settings->max_processes);
            if ($process_cursor->count() > $max) {
                $this->getHelper('json')->sendJson(array(
                    'message' => "Reach maximum processes per user ( " . $max . " )",
                    'result' => false
                ));
            }

            $process = array();
            $process['type'] = 'dataset';
            $process['user_id'] = $user['id'];
            $process['status'] = 'active';
            $process['classifier_id'] = null;
            $process['dataset_id'] = $dataset_id;
            $process['start_time'] = new MongoDate();
            $process['end_time'] = null;
            $process['program_id'] = $program['id'];
            $process['output'] = array();

            if (!isset($program['arguments'])) {
                $program['arguments'] = array();
            }

            $input_argments = array();
            foreach ($program['arguments'] as $arg) {
                $input_argments[] = array('name' => $arg['name'], 'value' => $arg['value']);
            }
            $process['arguments'] = $input_argments;

            $process_model->insert($process);
            $process_id = utf8_encode($process['_id']);

            $this->runProgram(0, $user['id'], $program, $dataset_id, $process_id, $input_argments, '0');
        }
    }

    public function runProgram($classifier_id, $user_id, $program, $dataset_id, $process_id, $input_arguments, $run_type = '0') {

        //// mark dataset as being processed, so that it can't be used by other programs
        $dataset_model = new Model_Datasets();
        $dataset_model->update(array("_id" => new MongoId($dataset_id)), array('$set' => array(
                "processing" => true
        )));

        /// Prepare arguments for program
        $extension = $program['language'] == 'c' ? '.out' : '.py'; /// c => out
        $program_full_path = APPLICATION_PATH . "/../" . "/programs/" . $program['type'] . "/" . $program['folder'] . "/" . $program['id'] . $extension;
        $dataset_full_path = APPLICATION_PATH . "/../" . "/user_data/" . $user_id . "/datasets/" . $dataset_id . ".csv";

        $user_output_path = APPLICATION_PATH . "/../" . '/user_data/' . $user_id . '/out/';

        if ($program['type'] == 'classifier') {
            $arguments = escapeshellarg($user_output_path) . " " . $process_id . " " . $classifier_id . " " . $run_type . " " . escapeshellarg($dataset_full_path);
        } else {
            $arguments = escapeshellarg($user_output_path) . " " . $process_id . " " . escapeshellarg($dataset_full_path);
        }

        foreach ($input_arguments as $argument) {
            $arguments .= " " . escapeshellarg($argument['value']);
        }

        if ($program['language'] == 'c') {
            $commandline = $program_full_path . " " . $arguments . " 2>&1";
        } else {
            $commandline = "python " . $program_full_path . " " . $arguments . " 2>&1";
        }

        $script_dir = "php " . APPLICATION_PATH . "/../" . "/job_engine/job.php";
        $command = $script_dir . " \"" . $commandline . "\" " . $process_id . " " . $dataset_id;

        $pid = exec("nohup $command > /dev/null 2>&1 & echo $!");

        $process_model = new Model_Processes();
        $setPidResult = $process_model->update(array("_id" => new MongoId($process_id)), array('$set' => array(
                "pid" => $pid
        )));

        if ($setPidResult) {
            $this->getHelper('json')->sendJson(array("result" => true));
        } else {
            $this->getHelper('json')->sendJson(array("result" => false));
        }
    }

    public function getprogramdetailsAction() {

        $program_model = new Model_Programs();
        $folder_model = new Model_Folders();
        $process_model = new Model_Processes();

        $folder_cursor = $folder_model->find()->sort(array('index' => 1));

        $classifierFolders = array();
        $datasetFolders = array();

        $totalClassifierProcess = 0;
        $totalDatasetProcess = 0;
        foreach ($folder_cursor as $folder) {
            $folder['id'] = utf8_encode($folder['_id']);
            $folder['key'] = $folder['index'];
            $program_cursor = $program_model->find(array('folder' => $folder['id']));
            $programs = array();

            $p = 0;
            foreach ($program_cursor as $program) {
                $program['id'] = utf8_encode($program['_id']);
                $program['key'] = $p;
                $program['full_name'] = $program['name'] . '.c';
                if ($program['language'] == 'python') {
                    $program['full_name'] = $program['name'] . '.py';
                }

                $process_cursor = $process_model->find(array('program_id' => $program['id'], 'status' => 'active'));

                $count = $process_cursor->count();
                $program['process_count'] = $count;

                if ($folder['type'] == 'classifier') {
                    $totalClassifierProcess += $count;
                } else {
                    $totalDatasetProcess += $count;
                }

                $programs[] = $program;
                $p++;
            }

            $folder['programs'] = $programs;

            if ($folder['type'] == 'classifier') {
                $classifierFolders[] = $folder;
            } else {
                $datasetFolders[] = $folder;
            }
        }

        $this->getHelper('json')->sendJson(array(
            'classifierFolders' => $classifierFolders,
            'datasetFolders' => $datasetFolders,
            'totalClassifier' => $totalClassifierProcess,
            'totalDataset' => $totalDatasetProcess
        ));
    }

}
