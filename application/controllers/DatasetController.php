<?php

class DatasetController extends Zend_Controller_Action {

    public function init() {
        
    }

    public function getallnamesAction() {

        $auth = Zend_Auth::getInstance();

        if ($auth->hasIdentity()) {
            $identity = $auth->getIdentity();
            $dataset_model = new Model_Datasets();
            $dataset_cursor = $dataset_model->find(array('user_id' => $identity['id'],'processing' => false), array("_id", "name"));
            
            $datasets = array();
            $d = 0;
            foreach ($dataset_cursor as $dataset) {
                $dataset['id'] = utf8_encode($dataset['_id']);
                $dataset['key'] = $d;
                $datasets[] = $dataset;
                $d++;
            }
            
            $this->getHelper('json')->sendJson($datasets);
        }

        $this->getHelper('json')->sendJson(array());
    }

    public function getbyidAction() {

        $dataset_model = new Model_Datasets();
        $dataset_id = $this->_request->getParam('id');
        $dataset_cursor = $dataset_model->findbyid($dataset_id);

        if (!$dataset_cursor) {
            $this->getHelper('json')->sendJson(array(
                'message' => "Dataset doens't exist",
                'result' => false
            ));
        }

        $dataset_cursor['id'] = utf8_encode($dataset_cursor['_id']);

        /// receive user
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $user = $auth->getIdentity();
        } else {
            $this->getHelper('json')->sendJson(array(
                'message' => "You are not authorized to load this dataset",
                'result' => false
            ));
        }

        $full_path = APPLICATION_PATH . "/../" . "/user_data/" . $user['id'] . "/datasets/" . $dataset_cursor['id'] . ".csv";

        if (!file_exists($full_path)) {
            $this->getHelper('json')->sendJson(array(
                'message' => "Dataset doens't exist",
                'result' => false
            ));
        }

        $file_handle = fopen($full_path, 'r');
        $count = 0;
        $file_corrupted = false;
        
        while (($row = fgetcsv($file_handle, 100000)) !== FALSE) {
            
            if(!$row){
                $file_corrupted = true;
                break;
            }
            
            $last = count($row);
            $feature_vector = implode(', ', array_slice($row, 1, $last));
            $label = $row[0];

            $dataset_data[] = array('feature_vector'=> $feature_vector, 'label' => $label);
            $count++;
            if ($count >= 20) {
                break;
            }
        }
        fclose($file_handle);

        $dataset_cursor['data'] = $dataset_data;
        
        $totalLines = intval(exec('wc -l ' . $full_path));
        $dataset_cursor['rows_total'] = $totalLines;
        
        $this->getHelper('json')->sendJson(array(
            'file_corrupted' => $file_corrupted,
            'dataset' => $dataset_cursor,
            'result' => true,
        ));
    }

    public function savenameAction() {

        if ($this->getRequest()->isPost()) {
            $dataset_model = new Model_Datasets();
            $dataset_id = $this->getRequest()->getPost('id');
            $newName = $this->getRequest()->getPost('name');

            $dataset_model->update(array("_id" => new MongoId($dataset_id)), array('$set' => array("name" => $newName)));

            $this->getHelper('json')->sendJson(array(
                'result' => true
            ));
        }
    }

    public function deletebyidAction() {

        if ($this->getRequest()->isPost()) {
            $dataset_model = new Model_Datasets();
            $dataset_id = $this->getRequest()->getPost('id');

            /// receive user
            $auth = Zend_Auth::getInstance();
            if ($auth->hasIdentity()) {
                $user = $auth->getIdentity();
            } else {
                $this->getHelper('json')->sendJson(array('result' => false));
            }

            $source_path = APPLICATION_PATH . "/../" . "/user_data/" . $user['id'] . "/datasets/" . $dataset_id . ".csv";

            if (!file_exists($source_path)) {
                $this->getHelper('json')->sendJson(array(
                    'result' => false
                ));
            }

            unlink($source_path);

            if ($dataset_model->delete($dataset_id)) {
                $this->getHelper('json')->sendJson(array("result" => true));
            }

            $this->getHelper('json')->sendJson(array("result" => false));
        }
    }

    public function duplicateAction() {

        if ($this->getRequest()->isPost()) {
            $dataset_model = new Model_Datasets();
            $dataset_id = $this->getRequest()->getPost('id');
            $name = $this->getRequest()->getPost('name');
            $dataset_cursor = $dataset_model->findbyid($dataset_id);

            if (!$dataset_cursor) {
                $this->getHelper('json')->sendJson(array('result' => false));
            }

            /// receive user
            $auth = Zend_Auth::getInstance();
            if ($auth->hasIdentity()) {
                $user = $auth->getIdentity();
            } else {
                $this->getHelper('json')->sendJson(array('result' => false));
            }

            $source_path = APPLICATION_PATH . "/../" . "/user_data/" . $user['id'] . "/datasets/" . $dataset_id . ".csv";

            if (!file_exists($source_path)) {
                $this->getHelper('json')->sendJson(array(
                    'message' => "The current dataset doens't exit.",
                    'result' => false
                ));
            }

            /// insert new dataset
            $duplicate = array();
            $duplicate['date'] = new MongoDate();
            $duplicate['name'] = $name;
            $duplicate['user_id'] = $dataset_cursor['user_id'];

            $dataset_model->insert($duplicate);
            $duplicate['id'] = utf8_encode($duplicate['_id']);

            $des_path = APPLICATION_PATH . "/../" . "/user_data/" . $user['id'] . "/datasets/" . $duplicate['id'] . ".csv";

            if (file_exists($des_path)) {
                $this->getHelper('json')->sendJson(array(
                    /// not true, same id instead, this should not occur
                    'message' => "There is a dataset with the same name.",
                    'result' => false
                ));
            }

            if (!copy($source_path, $des_path)) {
                $this->getHelper('json')->sendJson(array(
                    'message' => "Can't duplicate." . $des_path,
                    'result' => false
                ));
            }

            $this->getHelper('json')->sendJson(array(
                'id' => $duplicate['id'],
                'result' => true
            ));
        }
    }

    public function mergeAction() {

        if ($this->getRequest()->isPost()) {
            
            $dataset_model = new Model_Datasets();
            $dataset_id = $this->getRequest()->getPost('id');
            $merge_id = $this->getRequest()->getPost('merge_id'); /// id of dataset to merge with
            $dataset_cursor = $dataset_model->findbyid($dataset_id);
            $merge_cursor = $dataset_model->findbyid($merge_id);
            
            if (!$dataset_cursor || !$dataset_cursor) {
                $this->getHelper('json')->sendJson(array('result' => false));
            }

            /// receive user
            $auth = Zend_Auth::getInstance();
            if ($auth->hasIdentity()) {
                $user = $auth->getIdentity();
            } else {
                $this->getHelper('json')->sendJson(array('result' => false));
            }
            
            $first_dataset_path = APPLICATION_PATH . "/../" . "/user_data/" . $user['id'] . "/datasets/" . $dataset_id . ".csv";
            $second_dataset_path = APPLICATION_PATH . "/../" . "/user_data/" . $user['id'] . "/datasets/" . $merge_id . ".csv";
            
            if (!file_exists($first_dataset_path) || !file_exists($second_dataset_path)) {
                $this->getHelper('json')->sendJson(array(
                    'message' => "The dataset doens't exit.",
                    'result' => false
                ));
            }

            /// count how many cols there are
            $file_1_handle = fopen($first_dataset_path, 'r');
            $line_1 = fgets($file_1_handle); /// read the first line
            $col_numb_1 = count(explode(',',$line_1));
            $file_2_handle = fopen($second_dataset_path, 'r');
            $line_2 = fgets($file_2_handle);
            $col_numb_2 = count(explode(',',$line_2));
            
            if($line_1 != $line_2){
                $this->getHelper('json')->sendJson(array(
                    'message' => "Two datasets don't have same length of feature vector.",
                    'result' => false
                ));
            }
            
            $file_1_handle = fopen($first_dataset_path, 'a');
            
            fwrite($file_1_handle, PHP_EOL.$line_2);
            while (!feof($file_2_handle)) {
                $line = fgets($file_2_handle);
                fwrite($file_1_handle, $line);
            }
            fclose($file_2_handle);
            fclose($file_1_handle);
            
            $this->getHelper('json')->sendJson(array(
                'result' => true
            ));
        }
    }
    
    public function downloadAction() {

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $auth = Zend_Auth::getInstance();

            if ($auth->hasIdentity()) {
                $identity = $auth->getIdentity();

                $full_path = APPLICATION_PATH . "/../" . "/user_data/" . $identity['id'] . "/datasets/" . $formData['id'] . ".csv";
                if (!file_exists($full_path)) {
                    exit;
                }

                header("Pragma: public");
                header("Cache-Control: max-age=0");
                header("Content-Type: application/octet-stream");
                header("Content-Type: application/force-download", FALSE);
                header("Content-Type: application/download");
                header("Content-Disposition: attachment; filename=" . basename($full_path));
                header("Content-Transfer-Encoding: binary");
                readfile($full_path);
                exit;
            }
            exit;
        }
    }

    /*
     *  
     */
    public function preparetodrawAction() {
        if ($this->getRequest()->isPost()) {
            $dataset_model = new Model_Datasets();
            $dataset_id = $this->_request->getParam('id');
            $dataset_cursor = $dataset_model->findbyid($dataset_id);

            if (!$dataset_cursor) {
                $this->getHelper('json')->sendJson(array(
                    'message' => "Dataset doens't exist",
                    'result' => false
                ));
            }

            $dataset_cursor['id'] = utf8_encode($dataset_cursor['_id']);

            /// receive user
            $auth = Zend_Auth::getInstance();
            if ($auth->hasIdentity()) {
                $user = $auth->getIdentity();
            } else {
                $this->getHelper('json')->sendJson(array(
                    'message' => "You are not authorized to load this dataset",
                    'result' => false
                ));
            }

            $full_path = APPLICATION_PATH . "/../" . "/user_data/" . $user['id'] . "/datasets/" . $dataset_cursor['id'] . ".csv";

            if (!file_exists($full_path)) {
                $this->getHelper('json')->sendJson(array(
                    'message' => "Dataset doens't exist",
                    'result' => false
                ));
            }

            $file_handle = fopen($full_path, 'r');
            $row_index = 0;
            $file_corrupted = false;

            while (($row = fgetcsv($file_handle, 100000)) !== FALSE) {
                $label = $row[0];

                if ($row_index == 0) {
                    $last = count($row);
                }

                $feature_vector = array_slice($row, 1, $last);
                $data[$label][] = $feature_vector;
                ++$row_index;
            }
            
            fclose($file_handle);

            $this->getHelper('json')->sendJson(array(
                'file_corrupted' => $file_corrupted,
                'data' => $data,
                'result' => true,
            ));
        }
    }
}