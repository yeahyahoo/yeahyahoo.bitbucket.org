<?php

class ProcessController extends Zend_Controller_Action {

    //// for pagination, returns rows per page, prev,next buttons.
    //// currently, only used for dataset archive tab
    public function getpageAction() {
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $rows = intval($formData['rows']);
            $page = intval($formData['page']);
            
            if ($page == 0){
                $page = 1;
            }
            
            $type = $formData['type'];

            $process_model = new Model_Processes();
            $process_cursor = $process_model->find(array('type' => $type,'status'=>array('$in'=>array('done','error'))))
                    ->limit($rows)->skip(($page - 1) * $rows)->sort(array('start_time'=>-1));

            if (!$process_cursor) {
                $this->getHelper('json')->sendJson(array(
                    'result' => false,
                ));
            }

            if ($page > 1) {
                $prev = $page - 1;
            } else {
                $prev = 0;
            }

            if ($page * $rows < $process_cursor->count()) {
                $next = $page + 1;
            } else {
                $next = 1;
            }
            
            $program_model = new Model_Programs();
            $processes = array();
            if ($type == 'dataset') {
                $model = new Model_Datasets();
                foreach ($process_cursor as $proc) {
                    $cursor = $model->findById($proc['dataset_id']);
                    $program = $program_model->findById($proc['program_id']);
                    
                    $proc['dataset_name'] = $cursor['name'];
                    $proc['program_name'] = $program['name'];
                    $proc['formated_start_time'] = date('m/d/Y - h:i:s', $proc['start_time']->sec);
                                
                    if($proc['status']=='done'){
                        $sec_diff = $proc['end_time']->sec - $proc['start_time']->sec;
                        $min_str = round($sec_diff/60,1);
                        $proc['duration'] = $min_str;
                    }else{
                        $proc['duration'] = '';
                    }
                    $processes[] = $proc;
                }
            }

            $this->getHelper('json')->sendJson(array(
                'processes' => $processes,
                'prev' => $prev,
                'page' => $page,
                'next' => $next,
                'result' => true,
            ));
        }
    }

    public function getactiveAction() {

        $type = $this->getRequest()->getParam('type');
        $process_model = new Model_Processes();

        /// receive user
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $user = $auth->getIdentity();
        } else {
            $this->getHelper('json')->sendJson(array("result" => false));
        }

        $process_cursor = $process_model->find(array('user_id' => $user['id'], 'type' => $type ,'status'=> 'active'))->sort(array('start_time'=>-1));
        $processes = array();

        if ($type == 'dataset') {
            $model = new Model_Datasets();
            $program_model = new Model_Programs();
            foreach ($process_cursor as $proc) {
                $cursor = $model->findById($proc['dataset_id']);
                $proc['id'] = utf8_encode($proc['_id']);
                $proc['dataset_name'] = $cursor['name'];
                $proc['formated_start_time'] = date('m/d/Y - h:i:s', $proc['start_time']->sec);
                
                $program = $program_model->findById($proc['program_id']);
                $proc['program_name'] = $program['name'];
                
                $processes[] = $proc;
            }
        } else {
            $model = new Model_Classifiers();
            foreach ($process_cursor as $proc) {
                $cursor = $model->findById($proc['classifier_id']);
                $proc['id'] = utf8_encode($proc['_id']);
                $proc['classifier_name'] = $cursor['name'];
                $proc['formated_start_time'] = date('M/d/Y - h:i:s', $proc['start_time']->sec);
                $processes[] = $proc;
            }
        }

        $this->getHelper('json')->sendJson(array(
            "processes" => $processes,
            "result" => true
        ));
    }

    public function stopprocessAction() {
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            /// Check for authentication
            $auth = Zend_Auth::getInstance();
            if ($auth->hasIdentity()) {
                $user = $auth->getIdentity();
            } else {
                $this->getHelper('json')->sendJson(array("result" => false));
            }

            $process_model = new Model_Processes();
            $process = $process_model->findById($formData['id']);

            $pid = intval($process['pid']);
            if ($process['status'] == 'active' && $pid > 0) {
                exec("kill $pid");

                if (!$process_model->delete($formData['id'])) {
                    $this->getHelper('json')->sendJson(array("result" => false));
                }
                
                if($process['type'] == 'classifier'){
                    $classifier_model = new Model_Classifiers();
                    $classifier_model->update(array("_id" => new MongoId($process['classifier_id'])), array('$pull' => array('processes' => $formData['id'])));
                    //// clean up resources ( json, images)
                    
                    $user_output_path = APPLICATION_PATH . "/../" . "/user_data/" . $user['id'] . "/out/";
                    $json_file_path = $user_output_path . $formData['id'] . ".json";

                    if (file_exists($json_file_path)) {
                        $decoded_json = json_decode(file_get_contents($json_file_path));
                        foreach ($decoded_json as $output) {
                            if ($output->type == 'image_plot') {
                                $image_path = ($user_output_path . $output->data->name);
                                if(file_exists($image_path)){
                                    unlink($image_path);
                                }
                            }
                        }
                        unlink($json_file_path);
                    }
                }
                
                //// set dataset to not being processed
                $dataset_model = new Model_Datasets();
                $dataset_model->update(array("_id" => new MongoId($process['dataset_id'])), array('$set' => 
                    array('processing' => false)
                ));
                
                $this->getHelper('json')->sendJson(array("result" => true));
            }
        }
        $this->getHelper('json')->sendJson(array("result" => false));
    }
}

?>
