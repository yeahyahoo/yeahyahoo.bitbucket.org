<?php

class ClassifierController extends Zend_Controller_Action {

    public function init() {
        
    }

    public function getallnamesAction() {

        $auth = Zend_Auth::getInstance();

        if ($auth->hasIdentity()) {
            $identity = $auth->getIdentity();

            $classifier_model = new Model_Classifiers();
            $classifier_cursor = $classifier_model->find(array('user_id' => $identity['id']));
            $program_model = new Model_Programs();

            $classifiers = array();
            $c = 0;
            foreach ($classifier_cursor as $classifier) {
                $classifier['id'] = utf8_encode($classifier['_id']);
                $classifier['key'] = $c;
                $program_curcor = $program_model->findById($classifier['program_id']);
                $program_curcor['id'] = utf8_encode($program_curcor['_id']);
                $classifier['program'] = $program_curcor;
                $classifiers[] = $classifier;
                ++$c;
            }

            $this->getHelper('json')->sendJson($classifiers);
        }
        $this->getHelper('json')->sendJson(array());
    }

    public function getbyidAction() {

        $classifier_model = new Model_Classifiers();
        $process_model = new Model_Processes();
        $program_model = new Model_Programs();
        $dataset_model = new Model_Datasets();

        $classifier_id = $this->getRequest()->getParam('id');
        $classifier = $classifier_model->findbyid($classifier_id);
        
        $classifier['id'] = utf8_encode($classifier['_id']);
        
        $process_cursor = $process_model->find(array('classifier_id' => $classifier_id))->sort(array('start_time'=>-1));

        $program = $program_model->findbyid($classifier['program_id']);
        $classifier['program_name'] = $program['name'];

        $all_processes = array();
        foreach ($process_cursor as $process) {
            $dataset_cursor = $dataset_model->findbyid($process['dataset_id']);
            $process['dataset_name'] = $dataset_cursor['name'];
            $process['id'] = utf8_encode($process['_id']);
            $process['output'] = implode("\n", $process['output']);
            $process['formated_start_time'] = date('m/d/Y - h:i:s', $process['start_time']->sec);
            
            if($process['status']=='done'){
                $sec_diff = $process['end_time']->sec - $process['start_time']->sec;
                $min_str = round($sec_diff/60,1);
                $process['duration'] = $min_str;
            }else{
                $process['duration'] = '';
            }
            
            $all_processes[] = $process;
        }

        $classifier['processes'] = $all_processes;
        $this->getHelper('json')->sendJson($classifier);
    }

    public function savenameAction() {
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $classifier_model = new Model_Classifiers();
            $classifier_model->update(array("_id" => new MongoId($formData['id'])), array('$set' => array("name" => $formData['name'])));
            $this->getHelper('json')->sendJson(array("result" => true));
        }
        $this->getHelper('json')->sendJson(array("result" => false));
    }

    public function deletebyidAction() {
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $classifier_model = new Model_Classifiers();
            $process_model = new Model_Processes();

            try {
                $classifier_model->delete($formData['id']);
                $process_model->remove(array('classifier_id' => $formData['id']));
                $this->getHelper('json')->sendJson(array("result" => true));
            } catch (MongoCursorException $e) {
                $this->getHelper('json')->sendJson(array("result" => false));
            }
        }
        $this->getHelper('json')->sendJson(array("result" => false));
    }

    public function deleteprocessAction() {
        /// only delete classifier's process
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            /// Check for authentication
            $auth = Zend_Auth::getInstance();
            if ($auth->hasIdentity()) {
                $user = $auth->getIdentity();
            } else {
                $this->getHelper('json')->sendJson(array("result" => false));
            }
            
            $process_model = new Model_Processes();
            $process = $process_model->findById($formData['id']);
            
            if($process['type'] == 'classifier') {
                $classifier_model = new Model_Classifiers();
                $classifier_model->update(array("_id" => new MongoId($process['classifier_id'])), array('$pull' => array('processes' => $formData['id'])));
                
                //// clean up resources ( json, images)
                $user_output_path = APPLICATION_PATH . "/../" . "/user_data/" . $user['id'] . "/out/";
                $json_file_path = $user_output_path . $formData['id'] . ".json";

                if (file_exists($json_file_path)) {
                    $decoded_json = json_decode(file_get_contents($json_file_path));
                    foreach ($decoded_json as $output) {
                        if ($output->type == 'image_plot') {
                            $image_path = ($user_output_path . $output->data->name);
                            if (file_exists($image_path)) {
                                unlink($image_path);
                            }
                        }
                    }
                    unlink($json_file_path);
                }
            }

            //// delete process in database
            if ($process_model->delete($formData['id'])) {
                $this->getHelper('json')->sendJson(array("result" => true));
            }
        }
        $this->getHelper('json')->sendJson(array("result" => false));
    }
    
    /*
     *  The function is used to show all processes before drawing
     */
    public function getclassifierprocessesAction() {
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $process_model = new Model_Processes();

            $process_cursor = $process_model->find(array(
                        'classifier_id' => $formData['classifier_id'],
                        'status' => 'done',
                    ))->sort(array('end_time' => -1));

            $processes = array();
            $c = 0;
            foreach ($process_cursor as $process) {
                $process['id'] = utf8_encode($process['_id']);
                $process['key'] = $c;
                $process['formated_end_time'] = date('M/d/Y - h:i:s', $process['end_time']->sec);
                $processes[] = $process;
                ++$c;
            }

            $this->getHelper('json')->sendJson(array(
                "processes" => $processes,
                "result" => true
            ));
        }
        $this->getHelper('json')->sendJson(array("result" => false));
    }
    
    public function preparetodrawAction() {
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $user_output_path = APPLICATION_PATH . "/../" . "/user_data/" . $formData['user_id'] . "/out/";
            $json_file_path = $user_output_path . $formData['id'] . ".json";

            $has_json = file_exists($json_file_path);
            $decoded_json = null;
            if ($has_json) {
                $decoded_json = json_decode(file_get_contents($json_file_path));
                foreach ($decoded_json as $output) {
                    if ($output->type == 'image_plot') {
                        $image_file = base64_encode(file_get_contents($user_output_path . $output->data->name));
                        $output->data->src = "data:image/png;base64," . $image_file;
                    }
                }
            }

            $this->getHelper('json')->sendJson(array(
                "json" => $decoded_json,
                "has_json" => $has_json,
                "result" => true
            ));
        }
        $this->getHelper('json')->sendJson(array("result" => false));
    }

}
