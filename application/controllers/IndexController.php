<?php

class IndexController extends Zend_Controller_Action {

    public function init() {
        $this->view->layout()->setLayout('main');
    }

    public function indexAction() {
        
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/settings.ini', 'main');
        if($config->settings->construction == "true"){
            $this->redirect('/index.php/index/construction');
        }
        
        $auth = Zend_Auth::getInstance();
        
        if ($auth->hasIdentity()) {
            $identity = $auth->getIdentity();
        }
        
        if(isset($identity) && $identity['role']=='admin'){ // clear admin session
            Zend_Auth::getInstance()->clearIdentity();
            $this->redirect('/');
        }

        $this->view->brand = $config->settings->brand;
    }

    public function uploadAction() {
        
        $upload_form = new Form_User_Upload();
        $upload_form->setAction($this->view->baseUrl() . '/index.php/index/upload/');
        $this->view->upload_form = $upload_form;

        $uploadDir = APPLICATION_PATH . "/../"."/user_data/";

        $result = true;
        $auth = Zend_Auth::getInstance();
        if (!$auth->hasIdentity()) {
            $result = false;
        } else {
            $user = $auth->getIdentity();
        }
        
        $destination = $uploadDir. $user['id']."/datasets/";
        $upload_form->csv_file->setDestination($destination);

        if ($this->_request->isPost()) {
            $formData = $this->getRequest()->getPost();
            
            if(!$upload_form->isValid($formData)){
                $this->view->upload_msg = "Form is not valid";
                return;
            }
            $name = $formData['name'];
            
            if (!$upload_form->isValid($this->getRequest()->getParams())) {
                $this->view->upload_msg = "The file is not valid";
                $result = false;
            }

            if (!$upload_form->csv_file->receive()) {
                $this->view->upload_msg = "Can't receive the file.";
                $result = false;
            }

            if ($upload_form->csv_file->isUploaded()) {
                
                $dataset_model = new Model_Datasets();
                $dataset = array();
                $dataset['name'] = $name;
                $dataset['date'] = new MongoDate();
                $dataset['user_id'] = $user['id'];
                $dataset['processing'] = false;
                $dataset_model->insert($dataset);
                $dataset['id'] = utf8_encode($dataset['_id']);
                
                $old_file = $uploadDir . $user['id']. "/datasets/" . $_FILES['csv_file']['name'];
                $new_file = $uploadDir . $user['id']. "/datasets/" . $dataset['id'] . ".csv";
                
                /// do space to comma separation
                if($formData['csv_seperation']=='1'){
                    $file_handle = fopen($old_file, 'r');
                    $file_handle_write = fopen($new_file, 'w');
                    
                    while (!feof($file_handle)) {
                        $line = fgets($file_handle);
                        
                        $space_line = explode(' ',trim($line));
                        $comma_line = implode(',', $space_line);
                        
                        fwrite($file_handle_write, $comma_line.PHP_EOL);
                    }
                    
                    fclose($file_handle);
                    fclose($file_handle_write);
                    unlink($old_file);
                    
                }else{
                    rename($old_file, $new_file);
                }
                
            } else {
                $result = false;
            }

            if ($result) {
                $this->view->upload_msg = "Dataset is uploaded succesfully !";
            }
            
            $upload_form->reset();
        }
    }

    public function logoutAction() {
        Zend_Auth::getInstance()->clearIdentity();
        $this->redirect('/');
    }

    public function constructionAction(){
        
    }
}

?>