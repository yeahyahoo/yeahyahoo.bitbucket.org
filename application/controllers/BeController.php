<?php

class BeController extends Zend_Controller_Action {

    public function init() {
        $this->view->layout()->setLayout('admin');
    }

    public function indexAction() {
        $auth = Zend_Auth::getInstance();
        $identity = $auth->getIdentity();

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/settings.ini', 'main');
        $this->view->brand = $config->settings->brand;

        if (!$auth->hasIdentity()) {
            $this->_redirect("/index.php/be/login");
        }else{
            if ($identity['role'] != 'admin') {
                $this->_redirect("/");
            }
        }

        $this->view->admin = $identity;
    }

    public function loginAction() {

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/settings.ini', 'main');
        $this->view->brand = $config->settings->brand;

        if ($this->getRequest()->isPost()) {
            
            $model_admin = new Model_Admin();
            $authAdapter = new Zend_Auth_Adapter_MongoDb($model_admin, 'admin', 'password');

            $adminName = $this->_request->getPost('username');
            $password = $this->_request->getPost('password');

            if (trim($adminName) == '' || trim($password) == '') {
                $this->view->loginError = true;
                return;
            }

            $authAdapter->setIdentity($adminName);
            $authAdapter->setCredential(sha1($password . 'nakamura'));

            $auth = Zend_Auth::getInstance();
            $result = $auth->authenticate($authAdapter);

            if ($result->isValid()) {
                $identity = $authAdapter->getResultDocObject();
                $identity['id'] = utf8_encode($identity['_id']);
                unset($identity['_id']);
                unset($identity['password']);
                $identity['role'] = 'admin';
                $authStorage = $auth->getStorage();
                $authStorage->write($identity);
                
                $this->redirect("/index.php/be");
            }
            
            $this->view->loginError = true;
        }
    }

    public function logoutAction() {
        Zend_Auth::getInstance()->clearIdentity();
        $this->redirect("/index.php/be");
    }

    public function compilerAction() {
        $this->view->layout()->setLayout('compiler');
        $auth = Zend_Auth::getInstance();
        $this->view->admin = null;

        $identity = $auth->getIdentity();
        
        if (!$auth->hasIdentity() || $identity['role'] != 'admin') {
            $this->_redirect("/index.php/be/login");
        }
    }
    
    public function loadsettingsAction() {
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/settings.ini', 'main');

        $this->getHelper('json')->sendJson(array(
            'brand' => $config->settings->brand,
            'construction' => $config->settings->construction,
            'signup' => $config->settings->signup,
            'max_processes' => $config->settings->max_processes,
            'max_dataset_size' => $config->settings->max_dataset_size,
        ));
    }
    
    public function updatesettingsAction() {

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/settings.ini', 'main', array('skipExtends' => true,'allowModifications' => true));
            
            // Modify a value
            $config->settings->brand = $formData['brand'];
            $config->settings->construction = $formData['construction'];
            $config->settings->signup = $formData['signup'];
            $config->settings->max_processes = $formData['max_processes'];
            $config->settings->max_dataset_size = $formData['max_dataset_size'];

            // Write the config file
            $writer = new Zend_Config_Writer_Ini(array('config' => $config,
                'filename' => APPLICATION_PATH . '/configs/settings.ini'));
            $writer->write();
            
            $this->getHelper('json')->sendJson(array(
                'result' => true,
            ));
        }
    }
}
?>