function VisualizationController($scope, $state, Classifier, Dataset) {

    $scope.visual_tab = 'classifiers';
    $scope.visualization = {
        "graphs": [],
    };
    
    $scope.charts = [];

    $scope.classifierGraphs = [
        {
            'type': 'scatter',
            'name': 'Scatter plot',
        },
        {
            'type': 'histogram',
            'name': 'Histogram',
        },
        {
            type: 'line',
            name: 'Line chart'
        },
        {
            type: 'image_set',
            name: 'Images'
        },
        {
            type: 'tree',
            name: 'Tree'
        }
    ];
    
    $scope.datasetGraphs = [
        {
            'type': 'scatter',
            'name': 'Scatter plot',
        },
        {
            'type': 'histogram',
            'name': 'Histogram',
        },
        {
            type: 'line',
            name: 'Line chart'
        },
        {
            type: 'image_set',
            name: 'Images'
        }
    ];

    $scope.getClassifierNames();
    $scope.getDatasetNames();
}
