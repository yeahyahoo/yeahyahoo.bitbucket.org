function UserController($scope, $state, User) {
    
    $scope.current_password = null;
    $scope.new_password = null;
    $scope.retype_password = null;
    
    $scope.updatePassword = function(){
        User.changePassword($scope.current_password, $scope.new_password).success(function(data) {
            if(data.result){
                $scope.setAlert(1, "Updated !");
            }else{
                $scope.setAlert(2, data.message);
            }
        }).error(function() {
            $scope.setAlert(2, "Can't update.");
        });
    };
    
}