appModule.controller("GraphController", ["$scope","$state","$element",'Classifier','Dataset', 
    function( $scope,$start,$element,Classifier, Dataset ) {

    var thisChart;
    /*
    $scope.onResize = function() {
        if (thisChart) {
            thisChart.update();
        }
    };
    angular.element($element).resizable({handles: "n,s"});
    */
   
    $scope.remove = function($index) {
        $scope.setAlert(4, "Do you want to remove this graph ?", function() {
            $scope.visualization.graphs.splice($index, 1);
        });
    };

    $scope.redraw = function() {
        thisChart.update();
    };
    
    /*---------------------------------------------------------------------
     *  Drawing classifier outputs
     *  Types: scatter plot, histogram, line, image plot, tree, table
     * --------------------------------------------------------------------
     */
    $scope.selectedProcessIndex = null;
    $scope.selectedProcessOutput = null;
    $scope.processOutputs = [];
    if ($scope.graph.dataType == 'classifier'){
        Classifier.getClassifierProcesses($scope.graph.id).success(function(data){

            if( data.result){
                $scope.processes = data.processes;
                $scope.graph.show = 'settings';
                $scope.graph.isLoading = false;
            }else{
                $scope.graph.show = 'error';
            }
        }).error(function() { $scope.graph.show = 'error'; });
    }
    
    $scope.processOutputs = null;
    $scope.hasJson = false;
    $scope.changeProcess = function(){
        $scope.graph.isLoading = true;
        Classifier.prepareToDraw($scope.processes[$scope.selectedProcessIndex]).success(function(data){

            $scope.graph.isLoading = false;
            if( data.result){
                
                $scope.hasJson = data.has_json;
                if(data.has_json){
                    $scope.processOutputs = data.json;
                }
                
            }else{
                $scope.graph.show = 'error';
            }
        }).error(function() { $scope.graph.show = 'error'; });
    };

    $scope.showProcessOutput = function(){
        $scope.graph.show = 'body';
        $scope.graph.graphType = $scope.processOutputs[$scope.selectedProcessOutput].type;
        var json_data = $scope.processOutputs[$scope.selectedProcessOutput].data;
        var name = $scope.processOutputs[$scope.selectedProcessOutput].name;
        switch($scope.graph.graphType)
        {
            case 'scatter':
                $scope.drawScatterPlot();
                break;
            case 'histogram':
                $scope.drawHistogram();
                break;
            case 'image_plot':
                drawImagePlot();
                break;
            case 'line':
                break;
            case 'table':
                drawTable(name, json_data);
                break; 
            case 'tree':
                drawTree($element,name, json_data);
                break;            
        }
    };
    
    $scope.imagePlotSrc = '';
    $scope.plotHeading = null;
    function drawImagePlot() {
        $scope.imagePlotSrc = $scope.processOutputs[$scope.selectedProcessOutput].data.src;
        $scope.plotHeading = $scope.processOutputs[$scope.selectedProcessOutput].name;
    }
    
    //// draw table
    $scope.tableData = null;
    $scope.tableHeading = null;
    function drawTable(heading, data){
        $scope.tableHeading = heading;
        $scope.tableData = data;
    }
    
    /// draw tree
    $scope.treeHeading = null;
    function drawTree($element, heading, data){
        $scope.treeHeading = heading;
        drawD3Tree($element, data);
    }
    
    /*---------------------------------------------------------------------
     *  Drawing dataset
     *  Types: scatter plot, histogram, line chart, images
     *  -------------------------------------------------------------------
     */
    
    /*
     *  Loading data
     */
    if ($scope.graph.dataType == 'dataset') {
        Dataset.prepareToDraw($scope.graph.id).success(function(data) {

            if (data.result && !data.file_corrupted) {

                $scope.data = data.data;

                $scope.datasetLabels = [];
                for (var prop in $scope.data){
                    $scope.datasetLabels.push(prop);
                }
                
                $scope.first_point = $scope.data[$scope.datasetLabels[0]][0];
                
                $scope.graph.show = 'settings';
                $scope.graph.isLoading = false;
            } else {
                $scope.graph.show = 'error';
            }
        }).error(function() { $scope.graph.show = 'error'; });
    }
    
    /*
     *  Draw graph
     */

    $scope.drawDataset = function(){
        $scope.graph.show = 'body';
        switch($scope.graph.graphType)
        {
            case 'scatter':
                $scope.drawScatterPlot($scope.data, $scope.scatter.x , $scope.scatter.y );
                break;
            case 'histogram':
                $scope.drawHistogram($scope.data, $scope.hist.dim, $scope.hist.bin);
                break;
            case 'image_set':
                $scope.drawImages($scope.data);
                break;
            case 'line':
                $scope.drawLineChart($scope.data, $scope.line.x, $scope.line.y);
                break;
        }
    };
    
    /*
     * Scatter plot
     */
    $scope.scatter = {
        x: null,
        y: null
    };

    $scope.drawScatterPlot = function(data, x,y) {

        $graphBody = $($element).find(".graph-container").get(0);

        nv.addGraph(function() {
            var points = [];
            
            i = 0;
            for (var label in data) {
                points.push({
                    key: label,
                    values: []
                });
                for (j = 0; j < data[label].length; j++) {
                    points[i].values.push({
                        x: data[label][j][x],
                        y: data[label][j][y],
                        size: 1
                    });
                }
                ++i;
            }
            
            d3.select($($graphBody).find("svg").get(0)).remove();
            $($graphBody).prepend("<svg style='height:500px'></svg>");
            $svg = $($graphBody).find("svg").get(0);
            
            var chart = nv.models.scatterChart().showDistX(true).showDistY(true).color(d3.scale.category10().range());

            d3.select($svg).datum(points).transition().duration(500).call(chart);

            nv.utils.windowResize(chart.update);

            thisChart = chart;
            return chart;
        });
    };

    /*
     * Histogram
     */
    $scope.hist = {
        dim: null,
        bin: 20
    };

    $scope.drawHistogram = function(data, dimension, numb_bins) {

        $graphBody = $($element).find(".graph-container").get(0);

        var dimensionData = [];
        var histogramData = [];
        for (var key in data) {
            dimensionData[key] = data[key].map(function(d) {
                return d[dimension];
            });

            binnedData = d3.layout.histogram().bins(numb_bins)(dimensionData[key]);

            histogramData.push({
                key: key,
                values: binnedData,
            });
        }
        
        nv.addGraph(function() {

            d3.select($($graphBody).find("svg").get(0)).remove();
            $($graphBody).prepend("<svg style='height:500px'></svg>");
            $svg = $($graphBody).find("svg").get(0);

            var chart = nv.models.multiBarChart();
            chart.xAxis.tickFormat(d3.format(',f'));
            chart.yAxis.tickFormat(d3.format(',.1f'));

            d3.select($svg).datum(histogramData).transition().duration(500).call(chart);

            nv.utils.windowResize(chart.update);
            thisChart = chart;

            return chart;
        });
    };

    /*
     * Line chart
     */
    $scope.line = {
        x: null,
        y: null
    };

    $scope.drawLineChart = function(data, x, y) {

        $graphBody = $($element).find(".graph-container").get(0);

        nv.addGraph(function() {
            var points = [];

            i = 0;
            for (var label in data) {
                points.push({
                    key: label,
                    values: []
                });
                for (j = 0; j < data[label].length; j++) {
                    points[i].values.push({
                        x: data[label][j][x],
                        y: data[label][j][y],
                        size: 1
                    });
                }
                ++i;
            }

            d3.select($($graphBody).find("svg").get(0)).remove();
            $($graphBody).prepend("<svg style='height:500px'></svg>");
            $svg = $($graphBody).find("svg").get(0);

            var chart = nv.models.lineChart();
            chart.xAxis.tickFormat(d3.format('.02f'));
            chart.yAxis.tickFormat(d3.format('.02f'));
            d3.select($svg).datum(points).transition().duration(500).call(chart);

            nv.utils.windowResize(chart.update);

            thisChart = chart;
            return chart;
        });
    };

    /*
     * Images
     */
    $scope.selectedLabel = null;
    $scope.imageSet = {
        square:true,
        width:null
    };
}]);

function setPixel(imageData, x, y, r, g, b, a) {
    index = (x + y * imageData.width) * 4;
    imageData.data[index + 0] = r;
    imageData.data[index + 1] = g;
    imageData.data[index + 2] = b;
    imageData.data[index + 3] = a;
}

appModule.directive('datasetImageItem', function() {
  return function(scope, element, attrs) {
      
    var width;
    if(scope.imageSet.square){
        width = Math.floor(Math.sqrt(scope.first_point.length));
    }else{
        width = scope.imageSet.width;
    }
    
    $element = angular.element(element).attr('width',width.toString()).attr('height',width.toString());
    
    var img_data = scope.image;
    var canvas = $element.context;
    
    var context = canvas.getContext("2d");
    imageData = context.createImageData(width, width);
    
    for (y = width; y >= 0 ; y--) {
        for( x = 0; x< width; ++x){
            a = img_data[y+x*width];
            
            setPixel(imageData, y, x, a, a, a, 255); // 255 opaque
        }
    }

    // copy the image data back onto the canvas
    context.putImageData(imageData, 0, 0); // at coords 0,0

    canvas.style.width = '250px';
    canvas.style.height = '250px'; 
  };
});

function drawD3Tree($element, json_data) {

    var w = 1280, h = 800,i = 0,root;

    var tree = d3.layout.tree().size([h, w]);

    var diagonal = d3.svg.diagonal()
            .projection(function(d) {
        return [d.y, d.x];
    });

    var graph_tree = $($element).find(".graph-tree");
    graph_tree.find('svg').remove();
    var vis = d3.select(graph_tree.get(0))
            .call(d3.behavior.zoom().on("zoom", redraw))
            .append("svg:svg")
            .attr("width", w)
            .attr("height", h)
            .attr("pointer-events", "all")
            .append('svg:g');
  
    root = json_data;
    root.x0 = h / 2;
    root.y0 = 0;

    function toggleAll(d) {
        if (d.children) {
            d.children.forEach(toggleAll);
            toggle(d);
        }
    }
    update(root);

    function update(source) {
        var duration = d3.event && d3.event.altKey ? 5000 : 500;

        // Compute the new tree layout.
        var nodes = tree.nodes(root).reverse();

        // Normalize for fixed-depth.
        nodes.forEach(function(d) {
            d.y = d.depth * 180;
        });

        // Update the nodes…
        var node = vis.selectAll("g.node")

                .data(nodes, function(d) {
            return d.id || (d.id = ++i);
        });

        // Enter any new nodes at the parent's previous position.
        var nodeEnter = node.enter().append("svg:g")
                .attr("class", "node")
                .attr("id", function(d) {
            return 0
        })
                .attr("transform", function(d) {
            return "translate(" + source.y0 + "," + source.x0 + ")";
        })
                .on("click", function(d) {
            toggle(d);
            update(d);
        });

        nodeEnter.append("svg:rect")
                .attr("x", -5)
                .attr("y", -5)
                .attr("rx", 1)
                .attr("ry", 1)
                .attr("width", 15)
                .attr("height", 15)
                .style("fill", function(d) {
            return d._children ? "lightsteelblue" : "#fff";
        });

        nodeEnter.append("svg:text")
                .attr("y", "-1em")
                .attr("text-anchor", function(d) {
            return d.children || d._children ? "end" : "start";
        })
                .text(function(d) {
            return d.name;
        })
                .style("fill-opacity", 1e-6);

        // Transition nodes to their new position.
        var nodeUpdate = node.transition()
                .duration(duration)
                .attr("transform", function(d) {
            return "translate(" + d.y + "," + d.x + ")";
        });

        nodeUpdate.select("rect")
                .style("fill", function(d) {
            return d._children ? "lightsteelblue" : "#fff";
        });

        nodeUpdate.select("text")
                .style("fill-opacity", 1);

        // Transition exiting nodes to the parent's new position.
        var nodeExit = node.exit().transition()
                .duration(duration)
                .attr("transform", function(d) {
            return "translate(" + source.y + "," + source.x + ")";
        })
                .remove();

        nodeExit.select("rect")

        nodeExit.select("text")
                .style("fill-opacity", 1e-6);

        // Update the links…
        var link = vis.selectAll("path.link")
                .data(tree.links(nodes), function(d) {
            return d.target.id;
        });

        // Enter any new links at the parent's previous position.
        link.enter().insert("svg:path", "g")
                .attr("class", "link")
                .attr("d", function(d) {
            var o = {x: source.x0, y: source.y0};
            return diagonal({source: o, target: o});
        })
                .transition()
                .duration(duration)
                .attr("d", diagonal);

        // Transition links to their new position.
        link.transition()
                .duration(duration)
                .attr("d", diagonal);

        // Transition exiting nodes to the parent's new position.
        link.exit().transition()
                .duration(duration)
                .attr("d", function(d) {
            var o = {x: source.x, y: source.y};
            return diagonal({source: o, target: o});
        })
                .remove();

        // Stash the old positions for transition.
        nodes.forEach(function(d) {
            d.x0 = d.x;
            d.y0 = d.y;
        });

    }
    
    function toggle(d) {

        if (d.children) {
            d._children = d.children;
            d.children = null;
        } else {
            d.children = d._children;
            d._children = null;
        }
    }

    function redraw() {
        vis.attr("transform",
                "translate(" + d3.event.translate + ")"
                + " scale(" + d3.event.scale + ")");
    }
    
}
