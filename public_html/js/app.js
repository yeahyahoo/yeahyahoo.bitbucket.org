var appModule = angular.module('app', ['programServices', 'classifierServices', 'datasetServices', 'processServices']).config(['$stateProvider', '$routeProvider', '$urlRouterProvider',
    function($stateProvider, $routeProvider, $urlRouterProvider) {
        //$urlRouterProvider.when('/app', '/app/classifiers/new');
        $urlRouterProvider.when('/app/datasets/', '/app/datasets/browse');

        $stateProvider.state('app', {
            url: '/app',
            views: {
                "main-view": {
                    templateUrl: baseUrl + '/templates/app.html',
                    controller: ['$scope', '$state', 'Program', 'Classifier', 'Dataset', 'User', AppController]
                }
            }
        }).state('app.classifiers', {
            parent: 'app',
            url: '/classifiers/:subNav',
            templateUrl: baseUrl + '/templates/classifiers.html',
            controller: ['$scope', '$state', '$stateParams', '$timeout', 'Program', 'Classifier', 'Process', ClassifiersController]
        }).state('app.classifiers.detail', {
            parent: 'app',
            url: '/classifiers/browse/:id',
            templateUrl: baseUrl + '/templates/classifiers.html',
            controller: ['$scope', '$state', '$stateParams', '$timeout', 'Program', 'Classifier', 'Process', ClassifiersController]
        }).state('app.visualization', {
            parent: 'app',
            url: '/visualization',
            templateUrl: baseUrl + '/templates/visualization.html',
            controller: ['$scope', '$state', 'Classifier', 'Dataset', VisualizationController]
        }).state('app.datasets', {
            parent: 'app',
            url: '/datasets/:subNav',
            templateUrl: baseUrl + '/templates/datasets.html',
            controller: ['$scope', '$state', '$stateParams', '$timeout', 'Dataset', 'Program', 'Process', DatasetsController]
        }).state('app.datasets.detail', {
            parent: 'app',
            url: '/datasets/browse/:id',
            templateUrl: baseUrl + '/templates/datasets.html',
            controller: ['$scope', '$state', '$stateParams', '$timeout', 'Dataset', 'Program', 'Process', DatasetsController]
        }).state('app.user', {
            parent: 'app',
            url: '/user',
            templateUrl: baseUrl + '/templates/user.html',
            controller: ['$scope', '$state', 'User', UserController]
        });

    }]).run(['$rootScope', '$state', '$stateParams',
    function($rootScope, $state, $stateParams) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
    }]);

appModule.directive('droppable', function() {
    return {
        link: function(scope, element, attrs) {
            element.droppable({
                accept: ".draggable",
                hoverClass: 'widget-placeholder',
                drop: function(event, ui) {
                    var dataIndex = angular.element(ui.draggable).data('index');
                    var dataType = angular.element(ui.draggable).data('type');
                    
                    var visual_data = null;
                    if(dataType==='classifier'){
                        visual_data = scope.$parent.classifiers[dataIndex];
                    }else if(dataType==='dataset'){
                        visual_data = scope.$parent.datasets[dataIndex];
                    }
                    
                    scope.visualization.graphs.unshift({
                        title: visual_data.name,
                        show: 'settings',
                        isLoading: true,
                        attributes: {},
                        dataType: dataType,
                        id: visual_data.id
                    });

                    scope.$apply();
                }
            });
        }
    };
});

appModule.directive('draggable', function() {
    return {
        // A = attribute, E = Element, C = Class and M = HTML Comment
        restrict: 'A',
        link: function(scope, element, attrs) {

            element.draggable({
                handle: ".draggable-handle",
                revert: true,
                helper: 'clone',
                start: function(event, ui) {
                    $(ui.helper).addClass("ui-draggable-helper");
                },
                stop: function(event, ui) {
                }
            });

            element.disableSelection();
        }
    };
});

appModule.directive('sortable', function() {
    return {
        // A = attribute, E = Element, C = Class and M = HTML Comment
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.sortable({
                helper: 'clone',
                handle: ".draggable-handle",
                forcePlaceholderSize: true,
                forceHelperSize: true,
                containment: 'document',
                start: function(event, ui) {
                    //$(ui.helper).css('width',100);
                }
            });
            element.disableSelection();
        }
    };
});

appModule.directive('selectDropdown', function() {
    return {
        restrict: 'E',
        scope: {// set up directive's isolated scope
            list: "=", // amount var passed by reference (two-way)
            selectedKey: "=",
            change: "&",
        },
        replace: true,
        templateUrl: baseUrl + '/templates/select_dropdown.html',
        link: function(scope, element, attrs) {

            if (!scope.selectedName) {
                scope.selectedName = 'Choose...';
            }

            scope.itemClick = function(item) {
                scope.selectedKey = item.key;
                scope.selectedName = item.name;
            };

            scope.$watch('selectedKey', function(newValue, oldValue) {
                if (newValue !== oldValue) {
                    scope.change();
                }
            });

            scope.setClass = function(item) {
                if (scope.selectedKey === item.key) {
                    scope.selectedName = item.name;
                    return 'active';
                }

                return '';
            };
        }
    };
});

function AppController($scope, $state, Program, Classifier, Dataset, User) {

    /// global variables of app
    $scope.classifierFolders = [];
    $scope.datasetFolders = [];
    $scope.classifiers = null;
    $scope.datasets = null;
    
    /// load user's classifiers
    $scope.getClassifierNames = function() {
        Classifier.getAllNames().success(function(data) {
            $scope.classifiers = data;
        }).error(function(data) {
            $scope.setAlert(2, "Can't load classifiers");
            $scope.classifiers = [];
        });
    };
    $scope.getClassifierNames();

    $scope.getDatasetNames = function() {
        Dataset.getAllNames().success(function(data) {
            $scope.datasets = data;
            $scope.$broadcast('loadDatasets');
        }).error(function() {
            $scope.setAlert(2, "Can't load datasets");
            $scope.datasets = [];
        });
    };
    $scope.getDatasetNames();

    Program.getAll().success(function(data) {
        $scope.classifierFolders = data.classifierFolders;
        $scope.datasetFolders = data.datasetFolders;

    }).error(function() {
        $scope.setAlert(2, "Can't load programs");
    });

    $scope.clearHashkey = function(ngObj) {
        var output;
        output = angular.toJson(ngObj);
        output = angular.fromJson(output);
        return output;
    };
    
    /*
     *  Create alert modal
     */
    $scope.alertMessage = '';
    $scope.showAlert = 0;
    
    $scope.setAlert = function(type, msg, callback) {
        $scope.alertMessage = msg;
        $scope.alertShow = type;

        if (type === 4) {
            $scope.alertCallback = callback;
        } else {
            $scope.alertCallback = function() {
            };
        }

        $("#alertModal").modal('show');
    };
    $scope.formatTime = function(seconds) {
        var start_date = new Date(seconds * 1000);
        return (start_date.getMonth() + 1) + "/" + start_date.getDate() + "/" + start_date.getFullYear() + " - " + start_date.getHours() + ":" + start_date.getMinutes();
    };

}
