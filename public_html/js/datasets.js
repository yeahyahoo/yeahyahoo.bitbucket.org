function DatasetsController($scope, $state, $stateParams, $timeout, Dataset, Program, Process) {

    $scope.datasetNav = $stateParams.subNav;

    /*
     * Show selected Dataset
     */
    $scope.selectedDatasetIndex = null;
    $scope.selectedDataset = null;
    
    $scope.datasetDialog = '';
    
    $scope.changeDataset = function() {
        
        $scope.selectedDataset = $scope.datasets[$scope.selectedDatasetIndex];
        $state.transitionTo('app.datasets.detail', {
            id: $scope.selectedDataset.id
        });
    };

    function openDataset(id){
        $scope.datasetNav = 'browse';
        Dataset.getById(id).success(function(data) {
            if(data.result){
                $scope.selectedDataset = data.dataset;
                
                if($scope.selectedDataset.processing){
                    $scope.datasetDialog = 'processing';
                }else{
                    $scope.datasetDialog = 'detail';
                }
                
                if(data.file_corrupted){
                    $scope.setAlert(2, "There is one or more empty lines in the file.");
                }
            }else{
                $scope.setAlert(2, data.message);
            }
        }).error(function() {
            $scope.setAlert(2, "Can't load dataset");
        });        
    }

    if ($state.params.id !== undefined && $state.params.id !=="") {
        openDataset($state.params.id);
    }
    
    /*
     *  Rename dataset
     */
    $scope.renameDataset = false;

    $scope.closeRename = function() {
        $scope.renameDataset = false;
        
        Dataset.saveName($scope.selectedDataset.id, $scope.selectedDataset.name).success(function(data) {
            $scope.getDatasetNames();
        }).error(function() {
            $scope.setAlert(2, "Can't load dataset");
            $scope.datasets = [];
        });
    }

    /*
     *  Delete dataset
     */
    $scope.removeDataset = function() {
        $scope.setAlert(4, "Do you want to delete this dataset ?", function() {

            Dataset.deleteById($scope.selectedDataset.id).success(function(data) {
                $scope.getDatasetNames();

                $state.transitionTo('app.datasets', {
                    subNav: 'browse'
                });
                
            }).error(function() {
                $scope.setAlert(2, "Can't load dataset");
                $scope.datasets = [];
            });
        });
    }

    /*
     *  apply Programs
     */
    $scope.showDescription = false;
    $scope.selectedProgram = null;

    $scope.showModal = function(program) {
        $scope.selectedProgram = program;
        $("#programModal").modal('show');
    }

    $scope.applyProgram = function() {
        $scope.selectedProgram = $scope.clearHashkey($scope.selectedProgram);
        Program.runDatasetProgram($scope.selectedProgram, $scope.selectedDataset.id).success(function(data) {

            if (data.result) {
                $scope.setAlert(1, "The program has run.");
                $state.transitionTo('app.datasets', {
                    subNav: 'active'
                });
            }else{
                $scope.setAlert(2, data.message);
            }
            
        }).error(function() {
            $scope.setAlert(2, "Can't load dataset");
        });
    };

    /*
     *  Duplicate dataset
     */
    $scope.duplicateName = '';
    $scope.showDuplicateModal = function() {
        $("#duplicateModal").modal('show');
    };
    
    $scope.duplicateDataset = function() {

        $scope.duplicateName = $.trim($scope.duplicateName);
        if ($scope.duplicateName !== null && $scope.duplicateName !== '') {
            Dataset.duplicate($scope.selectedDataset.id, $scope.duplicateName).success(function(data) {
                if (data.result) {
                    $scope.getDatasetNames();
                    $state.transitionTo('app.datasets.detail', {
                        id: data.id
                    });
                    
                } else {
                    $scope.setAlert(2, "Can't duplicate dataset");
                }
            }).error(function() {
                $scope.setAlert(2, "Can't duplicate dataset");
            });
        }
    };

    /*
     *  Merge dataset
     */
    $scope.selectedMerge = null;
    $scope.showMergeModal = function() {
        $("#mergeModal").modal('show');
    };
    
    $scope.mergeDataset = function() {
        if ($scope.selectedMerge !== null) {
            Dataset.merge($scope.selectedDataset.id, $scope.selectedMerge).success(function(data) {
                if (data.result) {
                    openDataset($scope.selectedDataset.id);
                } else {
                    $scope.setAlert(2, data.message);
                }
            }).error(function() {
                $scope.setAlert(2, "Can't merge dataset");
            });
        }
    };
    
    /*
     *  Download dataset
     */
    $scope.downloadDataset = function(){
        $scope.datasetDialog = 'loading';
        Dataset.download($scope.selectedDataset.id).success(function(data) {
            $("#downloadBtn").attr("href", 'data:text/csv;charset=utf-8,' + escape(data))
            .attr("download", $scope.selectedDataset.name+".csv");
            $scope.datasetDialog = 'download';
        }).error(function() {$scope.setAlert(2, "Can't duplicate");});
    };
    
    /*
     *  Active jobs
     */
    $scope.activeProcesses = [];

    if ($scope.datasetNav === 'active') {
        getActiveProcesses();
    }
    
    function getActiveProcesses(){

        Process.getActiveProcesses('dataset').success(function(data) {
            if (data.result) {
                $scope.activeDatasetProcesses = data.processes;
            } else {
                $scope.setAlert(2, "Can't get active processes");
            }

            $timeout(getActiveProcesses, 30000);
        }).error(function() {
            $scope.setAlert(2, "Can't get active processes");
        });        
    }

    $scope.activeProcessIndex = null;
    
    $scope.assignActiveHover = function(index){
        $scope.activeProcessIndex = index;
    };

    $scope.stopProcess = function(process) {
        $scope.setAlert(4, "Do you want to stop this process ("+process.formated_start_time+") ?", function() {

            Process.stopProcess(process.id).success(function(data) {
                if (data.result) {
                    $scope.setAlert(1, "The process has been canceled.");
                    $scope.activeDatasetProcesses.splice($scope.activeProcessIndex, 1);
                } else {
                    $scope.setAlert(2, "Can't stop this process.");
                }
            }).error(function() {
                $scope.setAlert(2, "Can't stop this process.");
            });
        });
    };
        
    /*
     *  Dataset process archive
     */
    $scope.finishedProcesses = [];
    $scope.tableNext = 0;
    $scope.tablePage = 0;
    $scope.tablePrev = 0;
    
    $scope.getDoneProcesses = function(page){
        Process.getPage('dataset',page,20).success(function(data) {
            if (data.result) {

                $scope.finishedProcesses = data.processes;
                $scope.tableNext = data.next;
                $scope.tablePrev = data.prev;
                $scope.tablePage = data.page;

            } else {
                $scope.setAlert(2, "Can't load processes");
            }
        }).error(function() {
            $scope.setAlert(2, "Can't load processes");
        });
    };

    if ($scope.datasetNav === 'archive') {
        $scope.getDoneProcesses(1);
    }
        
}