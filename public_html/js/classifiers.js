function ClassifiersController($scope, $state, $stateParams, $timeout, Program, Classifier, Process) {

    $scope.classifierNav = $stateParams.subNav;
    
    /*
     *   New training
     */
    $scope.resetNewTraining = function() {
        $scope.newTraining = {};
        $scope.newTraining.dataset = null;
        $scope.newTraining.program = null;
        $scope.newTraining.classifier = null;
        $scope.newTraining.selectedFolder = null;
        $scope.newTraining.selectedProgram = null;
    };
    
    if($scope.classifierNav == 'new'){
        $scope.resetNewTraining();
    }
    
    $scope.classifierPrograms = [];
    $scope.selectFolder = function() {
        $scope.classifierPrograms = $scope.classifierFolders[$scope.newTraining.selectedFolder].programs;
    };
    
    $scope.selectProgram = function() {
         $scope.newTraining.program = angular.copy($scope.classifierPrograms[$scope.newTraining.selectedProgram]);
    };

    $scope.selectNewDataset = function() {
         $scope.newTraining.dataset = $scope.datasets[$scope.newTraining.selectedDataset];  
    };

    $scope.startNewTraining = function() {

        $scope.newTraining = $scope.clearHashkey($scope.newTraining);
        Program.newTraining($scope.newTraining).success(function(data) {
        });

        $scope.setAlert(1, "New training has been processes");
        $state.transitionTo('app.classifiers', {
            subNav: 'active'
        });
    };

    /*
     *  Continue training
     */
    $scope.resetContinueTraining = function() {
        $scope.continueTraining = {};
        $scope.continueTraining.dataset = null;
        $scope.continueTraining.program = null;
        $scope.continueTraining.classifier = null;
        $scope.continueTraining.selectedClassifier = null;
        $scope.continueTraining.selectedDataset = null;
    };

    if($scope.classifierNav == 'continue'){
        $scope.resetContinueTraining();
    }
    
    $scope.selectClassifier = function() {
        $scope.continueTraining.classifier = $scope.classifiers[$scope.continueTraining.selectedClassifier];
        $scope.continueTraining.program = angular.copy($scope.classifiers[$scope.continueTraining.selectedClassifier].program);
    };
    
    $scope.selectContinueDataset = function() {
         $scope.continueTraining.dataset = $scope.datasets[$scope.continueTraining.selectedDataset];  
    };

    $scope.startContinueTraining = function() {

        $scope.continueTraining = $scope.clearHashkey($scope.continueTraining);
        Program.continueTraining($scope.continueTraining).success(function(data) {

            if (data.result != false) {
                $scope.setAlert(1, "New training has been processes");
                $state.transitionTo('app.classifiers', {
                    subNav: 'active'
                });
            } else {
                $scope.setAlert(2, "Can't start.");
            }

        }).error(function() {
            $scope.setAlert(2, "Can't start.");
        });
    };
    
    /*
     *  Active
     */
    if($scope.classifierNav === 'active'){
        getActiveProcesses();
    }
    
    function getActiveProcesses(){
        Process.getActiveProcesses('classifier').success(function(data) {
            if(data.result){
                $scope.activeProcesses = data.processes;
                $timeout(getActiveProcesses, 30000);
            }else{
                $scope.setAlert(2, "Error.");
            }
        }).error(function() {
            $scope.setAlert(2, "Can't load active processes");    
        });        
    }
    
    $scope.activeProcessIndex = null;
    
    $scope.assignActiveHover = function(index){
        $scope.activeProcessIndex = index;
    };
    
    $scope.stopProcess = function(process) {
        $scope.setAlert(4, "Do you want to stop this process ("+process.formated_start_time+") ?", function() {

            Process.stopProcess(process.id).success(function(data) {
                if (data.result) {
                    $scope.setAlert(1, "The process has been canceled.");
                    $scope.activeProcesses.splice($scope.activeProcessIndex, 1);
                } else {
                    $scope.setAlert(2, "Can't stop this process.");
                }
            }).error(function() {
                $scope.setAlert(2, "Can't stop this process.");
            });
        });
    };

    /*
     *  /browse/:id
     */
    if($scope.classifierNav === 'browse'){
        $scope.getClassifierNames();
    }
    
    $scope.browseClassifierIndex = null;
    $scope.browseClassifier = null;
    $scope.changeClassifier = function() {
        var id = $scope.classifiers[$scope.browseClassifierIndex].id;
        $state.transitionTo('app.classifiers.detail', {
            id: id
        });
    };

    if ($state.params.id) {
        Classifier.getById($state.params.id).success(function(data) {

            $scope.classifierNav = 'browse';
            $scope.browseClassifier = data;
        }).error(function() {
            $scope.setAlert(2, "Error");
        });
    }

    $scope.removeClassifier = function() {
        $scope.setAlert(4,"Do you want to delete this classifier ?",function(){
            Classifier.deleteById($scope.browseClassifier.id).success(function(data) {
                $scope.getClassifierNames();
                $state.transitionTo('app.classifiers', {
                    subNav: 'browse'
                });
            }).error(function() {
                $scope.setAlert(2, "Error");
            });
        });
    };
    $scope.renameClassifier = false;
    $scope.closeRename = function() {
        $scope.renameClassifier = false;
        Classifier.saveName($scope.browseClassifier.id, $scope.browseClassifier.name).success(function(data) {
            $scope.getClassifierNames();
        }).error(function() {
            $scope.setAlert(2, "Error");
        });
    };
    
    $scope.openProcessModal = function(process){
        $scope.selected_process = process;
        $("#trainingModal").modal();
    };
    
    $scope.deleteProcess = function(process) {
        $scope.setAlert(4,"Do you want to delete this process ("+process.formated_start_time+") ?",function(){
            Classifier.deleteProcess($scope.browseClassifier.id, process.id).success(function(data) {
                if(data.result){
                    $scope.browseClassifier.processes.splice($scope.browseClassifier.hover, 1);
                }else{
                    $scope.setAlert(2, "Error.");
                }
            }).error(function() {
                $scope.setAlert(2, "Error.");
            });
        });
    };
    
}
