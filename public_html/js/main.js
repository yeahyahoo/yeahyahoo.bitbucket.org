/// Because we will host application in a sub folder. Therefore we need to setup base url. Default should be '/'.
/// This would affect stylesheet links, scripts, and request url
var baseUrl = '/~tlam';

var mainModule = angular.module('main', ['ui.compat', 'app', 'ui.validate', 'userServices']).config(['$stateProvider', '$routeProvider', '$urlRouterProvider',
    function($stateProvider, $routeProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');
        $stateProvider.state('main', {
            url: '/',
            views: {
                "main-view": {
                    templateUrl: baseUrl + '/templates/welcome.html',
                    controller: function($scope, $state) {

                    }
                }
            }
        });
    }]);

function MainController($rootScope, $scope, $state, User) {

    $scope.baseUrl = '/~tlam';
    $scope.userDialog = 0;
    $scope.Math = window.Math;
    $scope.Date = window.Date;
    $scope.frontMessage = null;
    
    $scope.user = {};

    User.checkStatus().success(function(data) {
        $scope.user = data;

        $scope.login = function() {
            User.login($scope.user.email, $scope.user.password).success(function(data) {
                if (data.result) {
                    $scope.loginError = false;
                    $scope.user.loggedIn = true;
                    $scope.frontMessage = null;
                    //$state.transitionTo('app');
                    window.location = $scope.baseUrl + "/#/app/classifiers/new";
                } else {
                    $scope.loginError = true;
                    $scope.frontMessage = "Wrong email or password !";
                }
            }).error(function() {
                alert("Error !");
            });
        };

        if (!$scope.user.loggedIn) {
            $state.transitionTo('main');
        } else {
            $scope.userDialog = 2;
        }
    }).error(function() {
        alert("Error !");
    });

    $scope.logout = function() {
        if (confirm("Do you want to logout ?")) {
            User.logout().success(function(data) {
                $state.transitionTo('main');
                $scope.userDialog = 0;
                $scope.user.email = '';
                $scope.user.loggedIn = false;
                $scope.frontMessage = null;
            });

        }
    };

    $scope.newUser = {
        emai: '',
        password: '',
        retype: ''
    };
    
    $scope.signUp = function() {
        User.signup($scope.newUser.email, $scope.newUser.password).success(function(data) {

            if (data.result) {
                $scope.user.loggedIn = true;
                $state.transitionTo('app');
                $scope.frontMessage = null;
            } else {
                $scope.frontMessage = "Email already exists";
            }
        });
    }
}
;

