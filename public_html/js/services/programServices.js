'use strict';

angular.module('programServices', ['ngResource']).factory('Program', function($http) {

    var requestUrl = baseUrl + '/index.php/program/';

    return {
        getById: function(id) {
            return $http.get(requestUrl + 'getbyid', {
                params: {
                    id: id
                }
            });
        },
        getAll: function() {
            return $http.get(requestUrl + 'getall');
        },
        newTraining: function(newTraining) {

            var data = $.param(newTraining);

            return $http({
                method: 'POST',
                url: requestUrl + 'newtraining',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            });
        },
        continueTraining: function(continueTraining) {

            var data = $.param(continueTraining);

            return $http({
                method: 'POST',
                url: requestUrl + 'continuetraining',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            });
        },
        runDatasetProgram: function(program, dataset_id) {
            var data = $.param({
                program: program,
                dataset_id: dataset_id
            });
            return $http({
                method: 'POST',
                url: requestUrl + 'rundatasetprogram',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        }
    };
});