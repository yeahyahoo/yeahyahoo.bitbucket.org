'use strict';

angular.module('datasetServices', []).factory('Dataset', ['$http', function($http) {

        var requestUrl = baseUrl + '/index.php/dataset/';

        return {
            getById: function(id) {
                return $http.get(requestUrl + 'getbyid', {
                    params: {
                        id: id
                    }
                });
            },
            getAllNames: function() {
                return $http.get(requestUrl + 'getallnames');
            },
            saveName: function(id, name) {

                var data = $.param({
                    id: id,
                    name: name,
                });

                return $http({
                    method: 'POST',
                    url: requestUrl + 'savename',
                    data: data,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                });
            },
            deleteById: function(id) {

                var data = $.param({
                    id: id,
                });

                return $http({
                    method: 'POST',
                    url: requestUrl + 'deletebyid',
                    data: data,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                });
            },
            duplicate: function(id, name) {
                var data = $.param({
                    id: id,
                    name: name
                });

                return $http({
                    method: 'POST',
                    url: requestUrl + 'duplicate',
                    data: data,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                });
            },
            merge: function(id, merge_id) {
                var data = $.param({
                    id: id,
                    merge_id: merge_id
                });

                return $http({
                    method: 'POST',
                    url: requestUrl + 'merge',
                    data: data,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                });
            },        
            download: function(id) {
                var data = $.param({
                    id: id,
                });
                return $http({
                    method: 'POST',
                    url: requestUrl + 'download',
                    data: data,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                });
            },        
            prepareToDraw: function(id) {
                var data = $.param({
                    id: id,
                });
                return $http({
                    method: 'POST',
                    url: requestUrl + 'preparetodraw',
                    data: data,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                });
            }
        };
    }]); 