'use strict';

angular.module('userServices', []).factory('User', ['$http',
function($http) {

	var requestUrl = baseUrl+'/index.php/user/';

	return {
		login : function(email, password) {

			var data = $.param({
				email : email,
				password : password,
			});

			return $http({
				method : 'POST',
				url : requestUrl + 'login',
				data : data,
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				},
			});
		},
		logout : function() {

			return $http({
				method : 'POST',
				url : requestUrl + 'logout',
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				},
			});
		},
		checkStatus : function() {
			return $http({
				method : 'POST',
				url : requestUrl + 'checkstatus',
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				},
			});
		},
		signup : function(email, password) {

			var data = $.param({
				email : email,
				password : password,
			});
			
			return $http({
				method : 'POST',
				url : requestUrl + 'signup',
				data : data,
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				},
			});
		},
		changePassword : function(current_password, new_password) {

			var data = $.param({
				current_password: current_password,
				new_password : new_password,
			});
			
			return $http({
				method : 'POST',
				url : requestUrl + 'changepassword',
				data : data,
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				},
			});
		},
	};
}]);
