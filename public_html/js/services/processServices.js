'use strict';

angular.module('processServices', []).factory('Process', ['$http', function($http) {

        var requestUrl = baseUrl + '/index.php/process/';

        return {
            getPage: function(type, page, rows) {
                var data = $.param({
                    type: type,
                    page: page,
                    rows: rows,
                });

                return $http({
                    method: 'POST',
                    url: requestUrl + 'getpage',
                    data: data,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                });
            },
            getActiveProcesses: function(type) {
                var data = $.param({
                    type: type
                });

                return $http({
                    method: 'POST',
                    url: requestUrl + 'getactive',
                    data: data,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                });
            },
            stopProcess: function(id) {
                var data = $.param({
                    id: id,
                });

                return $http({
                    method: 'POST',
                    url: requestUrl + 'stopprocess',
                    data: data,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                });
            }
        };
    }]); 