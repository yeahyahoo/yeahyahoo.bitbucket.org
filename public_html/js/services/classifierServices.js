'use strict';

angular.module('classifierServices', []).factory('Classifier', ['$http',
function($http) {

	/// /index.php/user/list
	var requestUrl = baseUrl+'/index.php/classifier/';

	return {
		getById : function(id) {
			return $http.get(requestUrl + 'getbyid', {
				params : {
					id : id
				}
			});
		},
		getAllNames : function() {
			return $http.get(requestUrl + 'getallnames');
		},
		saveName : function(id, name) {

			var data = $.param({
				id : id,
				name : name,
			});

			return $http({
				method : 'POST',
				url : requestUrl + 'savename',
				data : data,
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				},
			});
		},
		deleteById : function(id) {

			var data = $.param({
				id : id,
			});

			return $http({
				method : 'POST',
				url : requestUrl + 'deletebyid',
				data : data,
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				},
			});
		},
		deleteProcess : function(classifier_id, id) {

			var data = $.param({
				classifier_id : classifier_id,
				id : id,
			});

			return $http({
				method : 'POST',
				url : requestUrl + 'deleteprocess',
				data : data,
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			});
		},
		getClassifierProcesses : function(classifier_id) {

			var data = $.param({
				classifier_id : classifier_id,
			});

			return $http({
				method : 'POST',
				url : requestUrl + 'getclassifierprocesses',
				data : data,
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			});
		},
		prepareToDraw : function(process) {

			var data = $.param(process);

			return $http({
				method : 'POST',
				url : requestUrl + 'preparetodraw',
				data : data,
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			});
		}
	};

}]);
