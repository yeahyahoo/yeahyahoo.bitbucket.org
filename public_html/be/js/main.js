var baseUrl = '/~tlam';

angular.module('main', ['ui.compat', 'ui.validate', 'programServices', 'processServices', 'userServices','settingServices']).config(['$stateProvider', '$routeProvider', '$urlRouterProvider',
    function($stateProvider, $routeProvider, $urlRouterProvider) {
        
        $urlRouterProvider.otherwise('/programs');
        
        $stateProvider.state('programs', {
            url: '/programs',
            templateUrl: baseUrl + '/be/templates/programs.html',
            controller: ['$scope', '$state','Program', 'Process', ProgramController]
        }).state('programs_detail', {
            url: '/programs/:id',
            templateUrl: baseUrl + '/be/templates/programs.html',
            controller: ['$scope', '$state','Program', 'Process', ProgramController]
        }).state('users', {
            url: '/users',
            templateUrl: baseUrl + '/be/templates/users.html',
            controller: ['$scope', '$state', 'User', UserController]
        }).state('settings', {
            url: '/settings',
            templateUrl: baseUrl + '/be/templates/settings.html',
            controller: ['$scope', '$state', 'Setting', SettingController]
        });

    }]).run(['$rootScope', '$state', '$stateParams',
    function($rootScope, $state, $stateParams) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
}]);


function MainController($scope, $state) {

    $scope.baseUrl = '/~tlam';
    
    $scope.alertMessage = '';
    $scope.showAlert = 0;
    
    $scope.setAlert = function(type, msg, callback) {
        $scope.alertMessage = msg;
        $scope.alertShow = type;

        if (type === 4) {
            $scope.alertCallback = callback;
        } else {
            $scope.alertCallback = function() {
            };
        }

        $("#alertModal").modal('show');
    };    
}