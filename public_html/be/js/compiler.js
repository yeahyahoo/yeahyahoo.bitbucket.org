var baseUrl = '/~tlam';
var appModule = angular.module('app', ['compilerServices','programServices']);

appModule.directive('selectDropdown', function() {
    return {
        restrict: 'E',
        scope: {// set up directive's isolated scope
            list: "=", // amount var passed by reference (two-way)
            selectedKey: "=",
            change: "&",
        },
        replace:true,
        templateUrl: baseUrl +'/templates/select_dropdown.html',
        link: function(scope, element, attrs) {
            
            if(!scope.selectedName){
                scope.selectedName = 'Choose...';
            }
            
            scope.itemClick = function(item){
                scope.selectedKey = item.key;
                scope.selectedName = item.name;
            };
            
            scope.$watch('selectedKey', function(newValue, oldValue){
                if(newValue!==oldValue){
                    scope.change();
                }
            });
            
            scope.setClass = function(item) {
                if(scope.selectedKey === item.key){
                    scope.selectedName = item.name;
                    return 'active';
                }
                
                return '';
            };
        }
    };
});

function CompilerController($scope,$timeout, Compiler, Program) {

    $scope.programType = [
        { 
          key:'classifier',
          name: 'Classifier'
        },
        { 
          key:'dataset',
          name: 'Dataset'
        }
    ];
    
    $scope.programLanguages = [
        {
            key: 'c',
            name: '.c'
        },
        {
            key: 'python',
            name: '.py'
        }
    ];
    
    $scope.newProgram = {
        name: '',
        language: 'c',
    };
                
    $scope.program = {
        id: null,
        type: '',
        name: 'Program name',
        language: '',
        extension: '',
        arguments: [],
        description: ''
    };

    $scope.configuration = {
        theme: 'default',
        language: 'c'
    };
    
    /* Program dialog: close,  open, delete, loading
     */
    $scope.programPanel = 'close'; ///

    /* Message:  close, error, success, info
     */
    $scope.message = 0; ///

    /*
     *  Code mirror setup
     */
    var compilerArea = document.getElementById("compilerArea");

    var editor = CodeMirror.fromTextArea(compilerArea, {
        mode: {name: "python",
            version: 2,
            singleLineStringErrors: false},
        lineNumbers: true,
        indentUnit: 4,
        lineWrapping: true,
        showCursorWhenSelecting: true,
        vimMode: true,
        keyMap: "vim",
        extraKeys: {
            "Tab": function() {
                editor.replaceSelection("   ");
                editor.setCursor(editor.getCursor());
            },
        },
        onChange: function(cm) {
            cm.getValue();
        }
    });

    $('.CodeMirror').resizable({
        resize: function() {
            editor.setSize("auto", $(this).height());
        }
    });

    /*
     *  Change theme
     */
    /// load theme
    $scope.themes = [
        {key:'default',name:'default'},
        {key:'3024-night',name:'3024-night'},
        {key:'3024-day',name:'3024-day'},
        {key:'ambiance',name:'ambiance'},
        {key:'base16-dark',name:'base16-dark'},
        {key:'base16-light',name:'base16-light'},
        {key:'blackboard',name:'blackboard'},
        {key:'cobalt',name:'cobalt'},
        {key:'eclipse',name:'eclipse'},
        {key:'elegant',name:'elegant'},
        {key:'erlang-dark',name:'erlang-dark'},
        {key:'lesser-dark',name:'lesser-dark'},
        {key:'midnight',name:'midnight'},
        {key:'monokai',name:'monokai'},
        {key:'neat',name:'neat'},
        {key:'night',name:'night'},
        {key:'rubyblue',name:'rubyblue'},
        {key:'solarized dark',name:'solarized dark'},
        {key:'solarized light',name:'solarized light'},
        {key:'tomorrow-night-eighties',name:'tomorrow-night-eighties'},
        {key:'twilight',name:'twilight'},
        {key:'vibrant-ink',name:'vibrant-ink'},
        {key:'xq-dark',name:'xq-dark'},
        {key:'xq-light',name:'xq-light'},
    ];
    
    $scope.selectedTheme = '';

    Compiler.loadTheme().success(function(data) {
        if (data.result) {
            $scope.selectedTheme = data.theme;
        } else {
            $scope.setAlert(2, data.message);
        }
    }).error(function() {
        $scope.setAlert(2, "Can't change theme.");
    });

    $scope.selectTheme = function() {
        Compiler.updateTheme($scope.program.id, $scope.selectedTheme).success(function(data) {
            if (data.result) {
                editor.setOption("theme", $scope.selectedTheme);
            } else {
                $scope.setAlert(2, data.message);
            }
        }).error(function() {
            $scope.setAlert(2, "Can't change theme.");
        });
    };

    /*
     *  setAlert
     */
    $scope.setAlert = function(type, msg, callback) {
        $scope.alertMessage = msg;
        $scope.alertType = type;
        
        var currentTime = new Date();
        var hours = currentTime.getHours();
        var minutes = currentTime.getMinutes();
        
        if (hours < 10){
            hours = "0" + hours;
        }
        
        if (minutes < 10){
            minutes = "0" + minutes;
        }
        
        $scope.alertTime = hours + ":" + minutes;
    };
    
    $scope.classifierFolders = [];
    $scope.datasetFolders = [];    
    $scope.classifiers = [];
    $scope.datasets = [];

    $scope.selectedProgram = {
        id: null,
        type: null
    };
    
    $scope.openType = 'classifier';
    $scope.folderDialog = 'start';
    $scope.setOfPrograms = [];
    
    $scope.loadPrograms = function(){
        $scope.programPanel = 'loading';
        Program.getAll().success(function(data) {

            $scope.classifierFolders = data.classifierFolders;
            $scope.datasetFolders = data.datasetFolders;
        }).error(function() {
            $scope.setAlert(2, "Can't load programs.");
        });
    };
    
    /*
     *  Folder operations
     */
    $scope.newFolderName = null;
    $scope.selectedFolderIndex = null;
     
    $scope.createFolder = function(){
        
        $scope.newFolderName = $.trim($scope.newFolderName);
        if($scope.newFolderName===''){
            $scope.setAlert(2, "Please enter folder name.");
            return;
        }
        
        var index;
        if($scope.openType==='classifier'){
            index = $scope.classifierFolders.length;
        }else{
            index = $scope.datasetFolders.length;
        }
        
        Compiler.createFolder($scope.openType,$scope.newFolderName,index).success(function(data) {
            if(data.result){
                $scope.setAlert(1, data.message);
                
                if($scope.openType==='classifier'){
                    $scope.classifierFolders.push(data.folder);
                }else{
                    $scope.datasetFolders.push(data.folder);
                }
                $scope.newFolderName = '';
                $scope.folderDialog = 'start';
            }else{
                $scope.setAlert(2, data.message);
            }
        }).error(function() {$scope.setAlert(2, "Can't create a new folder.");});
    };
    
    $scope.showRenameFolderDialog = function() {
        if($scope.newFolderName!=null){
            if ($scope.openType === 'classifier') {
                $scope.newFolderName = $scope.classifierFolders[$scope.selectedFolderIndex].name;
            } else {
                $scope.newFolderName = $scope.datasetFolders[$scope.selectedFolderIndex].name;
            }
            $scope.folderDialog = 'rename';
        }else{
            $scope.setAlert(2,"Please select a folder.");
        }
    };

    $scope.deleteFolder = function() {

        if ($scope.openType === 'classifier') {
            folder = $scope.classifierFolders[$scope.selectedFolderIndex];
        } else {
            folder = $scope.datasetFolders[$scope.selectedFolderIndex];
        }

        $scope.setModalAlert(4, "Do you want to delete folder " + folder.name + " ?", function() {
            Compiler.deleteFolder(folder.id).success(function(data) {
                if (data.result) {
                    $scope.setAlert(1, data.message);

                    var programPanel = $scope.programPanel;
                    $scope.loadPrograms();
                    $scope.programPanel = programPanel;
                    $scope.selectedFolderIndex = null;
                    $scope.newFolderName = '';
                } else {
                    $scope.setAlert(2, data.message);
                }
            }).error(function() {
                $scope.setAlert(2, "Can't delete the folder.");
            });
        });
    };

    $scope.renameFolder = function(){
        
        if($scope.selectedFolderIndex===null){
            $scope.setAlert(2,"Please select a folder.");
            return;
        }
        
        var folder = null;
        if($scope.openType==='classifier'){
            folder = $scope.classifierFolders[$scope.selectedFolderIndex];
        }else{
            folder = $scope.datasetFolders[$scope.selectedFolderIndex];
        }
        
        Compiler.renameFolder(folder.id, $scope.newFolderName).success(function(data) {
            if(data.result){
                $scope.setAlert(1, data.message);
                if($scope.openType==='classifier'){
                    $scope.classifierFolders[folder.index].name = angular.copy($scope.newFolderName);
                }else{
                    $scope.datasetFolders[folder.index].name = angular.copy($scope.newFolderName);
                }
                
                $scope.folderDialog = 'start';
            }else{
                $scope.setAlert(2, data.message);
            }
        }).error(function() {
            $scope.setAlert(2, "Can't rename the folder.");
        });
    };
    
    $scope.folderMoveUp = function(){
        
        if($scope.selectedFolderIndex===null){
            $scope.setAlert(2,"Please select a folder.");
            return;
        }
        
        if($scope.selectedFolderIndex<=0){
            return;
        }
        
        var folderUp, folderDown;
        if($scope.openType==='classifier'){
            folderUp = $scope.classifierFolders[$scope.selectedFolderIndex];
            folderDown = $scope.classifierFolders[$scope.selectedFolderIndex-1];
            
            --folderUp.index;
            ++folderDown.index;
            var temp = angular.copy(folderUp);            
            $scope.classifierFolders[$scope.selectedFolderIndex] = folderDown;
            $scope.classifierFolders[$scope.selectedFolderIndex-1] = temp;
        
        }else{
            folderUp = $scope.datasetFolders[$scope.selectedFolderIndex];
            folderDown = $scope.datasetFolders[$scope.selectedFolderIndex-1];
            
            --folderUp.index;
            ++folderDown.index;
            var temp = angular.copy(folderUp);            
            $scope.datasetFolders[$scope.selectedFolderIndex] = folderDown;
            $scope.datasetFolders[$scope.selectedFolderIndex-1] = temp;
        }

        Compiler.folderMoveUp(folderUp.id, folderDown.id, $scope.selectedFolderIndex).success(function(data) {
            --$scope.selectedFolderIndex;
        }).error(function() {$scope.setAlert(2, "Error.");});
    };
    
    $scope.folderMoveDown = function(){
        
        if($scope.selectedFolderIndex===null){
            $scope.setAlert(2,"Please select a folder.");
            return;
        }
        var folderUp, folderDown;
        if($scope.openType==='classifier'){

            if($scope.selectedFolderIndex>($scope.classifierFolders.length-2) ){
                return;
            }

            folderUp = $scope.classifierFolders[$scope.selectedFolderIndex+1];
            folderDown = $scope.classifierFolders[$scope.selectedFolderIndex];
            
            --folderUp.index;
            ++folderDown.index;
            var temp = angular.copy(folderDown);            
            $scope.classifierFolders[$scope.selectedFolderIndex] = folderUp;
            $scope.classifierFolders[$scope.selectedFolderIndex+1] = temp;
        
        }else{
            if($scope.selectedFolderIndex>($scope.datasetFolders.length-2) ){
                return;
            }
            
            folderUp = $scope.datasetFolders[$scope.selectedFolderIndex+1];
            folderDown = $scope.datasetFolders[$scope.selectedFolderIndex];
            
            --folderUp.index;
            ++folderDown.index;
            var temp = angular.copy(folderDown);
            $scope.datasetFolders[$scope.selectedFolderIndex] = folderUp;
            $scope.datasetFolders[$scope.selectedFolderIndex+1] = temp;
        } 
        
        Compiler.folderMoveDown(folderUp.id, folderDown.id,$scope.selectedFolderIndex).success(function(data) {
        
        ++$scope.selectedFolderIndex;  
        }).error(function() {$scope.setAlert(2, "Error.");});
    };
    
    /*
     *  Folder visibility
     */
    $scope.selectedFolderVisibility = false;
    $scope.toggleFolderVisibility = function(){
        
        var folder;
        if($scope.openType==='classifier'){
            folder = $scope.classifierFolders[$scope.selectedFolderIndex];
        }else{
            folder = $scope.datasetFolders[$scope.selectedFolderIndex];
        }
        
        Compiler.folderVisibility(folder.id, !folder.show).success(function(data) {
            if(data.result){
                folder.show = !folder.show;
                $scope.selectedFolderVisibility = folder.show;
            }
        }).error(function() {$scope.setAlert(2, "Error.");});
    };
    
    /*
     *  Create new program
     */
    $scope.showNewPanel = function() {
        $scope.loadPrograms();
        $scope.resetOpenDialog();
        $scope.programPanel = 'new';
        
    };

    $scope.createNewProgram = function() {
        $scope.programPanel = 'loading';
        $scope.newProgram.type = $scope.openType;
        
        var folder;
        if($scope.openType==='classifier'){
            folder = $scope.classifierFolders[$scope.selectedFolderIndex];
        }else{
            folder = $scope.datasetFolders[$scope.selectedFolderIndex];
        }
        
        $scope.newProgram.folder = folder.id;
        Compiler.create($scope.newProgram).success(function(data) {
            if (data.result) {
                $scope.program = data.program;

                editor.setValue(data.content);
                
                $scope.setAlert(1, data.message);
                $scope.programPanel = 'close';

                $scope.program.extension = '.c';
                if ($scope.program.language === 'python') {
                    $scope.program.extension = '.py';
                }
                
                $scope.newProgram = {
                    name: '',
                    language: 'c',
                };
                
                $scope.showEditor = true;
            } else {
                $scope.setAlert(2, data.message);
                $scope.programPanel = 'new';
            }
        }).error(function() {
            $scope.setAlert(2, "Can't create a new program.");
        });
    };

    /*
     *  Open program
     */

    $scope.showOpenPanel = function() {
        $scope.loadPrograms();
        $scope.resetOpenDialog();
        $scope.programPanel = 'open';
    };
    
    $scope.openProgram = function() {
        
        $scope.programPanel = 'loading';
        $scope.selectedProgram.type = angular.copy($scope.openType);
        
        Compiler.open($scope.selectedProgram.id).success(function(data) {

            if (data.result) {
                $scope.program = data.program;
                editor.setValue(data.content);
                $scope.setAlert(1, data.message);
                $scope.programPanel = 'close';

                $scope.program.extension = '.c';
                if ($scope.program.language === 'python') {
                    $scope.program.extension = '.py';
                }                
                
                $scope.showEditor = true;
                $scope.terminalOutput = [];
                $scope.imageOutput = [];
                $scope.jsonOutput = '';
                
            } else {
                $scope.setAlert(2, data.message);
            }
        }).error(function() {
            $scope.setAlert(2, "Can't open the program.");
        });
    };
    
    $scope.resetOpenDialog = function(){
        $scope.folderDialog = 'start';
        $scope.newFolderName = '';
        $scope.selectedFolderIndex = null;
        $scope.selectedProgram = null;
        $scope.setOfPrograms = [];
    };
    
    $scope.changeFolder = function(){
        
        var selectedFolder;
        if($scope.openType==='classifier'){
            selectedFolder = $scope.classifierFolders[$scope.selectedFolderIndex];

            $scope.setOfPrograms = selectedFolder.programs;
        }else{
            selectedFolder = $scope.datasetFolders[$scope.selectedFolderIndex];

            $scope.setOfPrograms = selectedFolder.programs;
        }
        
        $scope.newFolderName = angular.copy(selectedFolder.name);
        $scope.selectedFolderVisibility = JSON.parse(selectedFolder.show);
    };
    /*
     *  Save program
     */
    $scope.clearHashkey = function(ngObj) {
        var output;
        output = angular.toJson(ngObj);
        output = angular.fromJson(output);
        return output;
    }
    
    $scope.saveProgram = function() {
        programPanel = 'loading';

        if ($scope.program.id === null) {
            $scope.setAlert(2, "Please create/open a program.");
            programPanel = 'close';
            return;
        }

        /// clear argument hashkey
        $scope.program = $scope.clearHashkey($scope.program);
        
        Compiler.save($scope.program, editor.getValue()).success(function(data) {

            if (data.result) {
                $scope.setAlert(1, data.message);
                $scope.programPanel = 'close';
            } else {
                $scope.setAlert(2, data.message);
            }
        }).error(function() {
            $scope.setAlert(2, "Can't save the program.");
        });
    }

    /*
     *  Rename file
     */
    $scope.editName = 0;
    $scope.newName = '';
    
    $scope.openFileRename = function(){
        $scope.editName = 1;
        $scope.newName = $scope.program.name;
    }
    
    $scope.changeFileName = function() {
        $scope.editName = 0;
        if($.trim($scope.newName)==''){
            return;
        }
        
        Compiler.rename($scope.program.id, $scope.newName).success(function(data) {
            if (data.result) {
                $scope.program.name = $scope.newName;
                $scope.setAlert(1, data.message);
            } else {
                $scope.setAlert(2, data.message);
            }
        }).error(function() {
            $scope.setAlert(2, "Can't change name.");
        });
    }
    /*
     *  show Arguments
     */
    $scope.newParm = {
        name: null,
        hint: null
    };

    $scope.addArgument = function() {
        for (var i = 0; i < $scope.program.arguments.length; ++i) {
            if ($scope.newParm.name === $scope.program.arguments[i].name) {
                $scope.setAlert(2, "There is a argument with the same name.");
                return;
            }
        }

        if ($scope.program.arguments.length < 10) {
            var new_parm = angular.copy($scope.newParm);
            $scope.program.arguments.push(new_parm);
            $scope.newParm = {
                name: null,
                hint: null
            };
        }
    }

    $scope.deleteParamter = function(i) {
        $scope.program.arguments.splice(i, 1);
    }
    
    /*
     *  Compile
     */
    $scope.terminalOutput = [];
    $scope.jsonOutput = '';
    $scope.imageOutput = [];
    
    var terminalArea = document.getElementById('terminalArea');
    var imageArea = document.getElementById('imageArea');
    
    CodeMirror.commands.save = function() {
        $scope.$apply($scope.saveProgram());
    };
    
    $scope.compileProgram = function(){
        compileAndRun();
    };
    
    var compileAndRun = function(){
        $scope.terminalOutput.push('---------------------');
        $scope.terminalOutput.push('Compiling...');
        
        /// clear argument hashkey
        $scope.program = $scope.clearHashkey($scope.program);
        $scope.programPanel = 'loading';
        Compiler.compile($scope.program, editor.getValue()).success(function(data) {
            if (data.result) {
                
                if(data.terminal.length>0){
                    $scope.terminalOutput.push('GCC output:');
                }
                
                for(n in data.terminal){
                    $scope.terminalOutput.push(data.terminal[n]);
                }
                
                if (data.terminalStatus == 0) {
                    $scope.setAlert(1, data.message);
                    $scope.showRunDialog();
                }else{
                    $scope.setAlert(2, "Program has compile error.");
                    $scope.programPanel = 'close';
                }
                
            } else {
                $scope.setAlert(2, data.message);
            }
            $scope.terminalOutput.push('Done!');
        }).error(function() {
            $scope.setAlert(2, "Can't compile the program.");
        });
        
        limitOutput();
        ouputStayAtBottom();
    };
    
    /*
     *  Run
     */
    $scope.runTypeIndex = 0;
    $scope.runTypes = [
        {
        key: 0,
        name: 'New',
        type: '0'
        }, {
        key: 1,
        name: 'Continue',
        type: '1'
        }
    ];

    $scope.datasetIndex = null;
    $scope.runArguments = [];

    $scope.showRunDialog = function() {
        $scope.programPanel = 'run';
        $scope.runArguments = angular.copy($scope.program.arguments);
    };
    
    $scope.runningProgram = {};
    $scope.runProgram = function(){
        $scope.programPanel = 'running';
        $scope.terminalOutput.push('---------------------');
        $scope.terminalOutput.push('Program is running...');
        
        $scope.program = $scope.clearHashkey($scope.program);
        var runType = '';

        if ($scope.program.type == 'classifier') {
            runType = $scope.runTypes[$scope.runTypeIndex].type;
        }

        var dataset_id = $scope.datasets[$scope.datasetIndex].id;
        var data_path = $scope.datasets[$scope.datasetIndex].path;
        Compiler.run($scope.program, runType, dataset_id, data_path, $scope.runArguments, editor.getValue()).success(function(data) {
            if (data.result) {
                $scope.runningProgram = data.program;
                $timeout(checkProgramStatus, 30000);
            } else {
                $scope.setAlert(2, data.message);
            }
        }).error(function() {
            $scope.setAlert(2, "Can't run the program.");
        });       
        
        limitOutput();
        ouputStayAtBottom();
    };
    
    /*
     *  Delete program
     */
    $scope.deleteProgram = function(){
        Compiler.delete($scope.program).success(function(data) {
            if (data.result) {
                $scope.program = {
                    id: null,
                    type: '',
                    name: 'Program name',
                    language: '',
                    extension: '',
                    arguments: [],
                    description: ''
                };
                
                $scope.programPanel = 'close';
                $scope.showEditor = false;
                $scope.setAlert(1, data.message);
            } else {
                $scope.setAlert(2, data.message);
            }
        }).error(function() {
            $scope.setAlert(2, "Can't delete the program.");
        });
    };
    
    function limitOutput(){
        /// limit max output line of all 3 output panels
        var len = $scope.terminalOutput.length;
        var max = 50;
        if(len>max){
            var deleteCount = len - max;
            for(i = 0; i < deleteCount; ++i ){
                $scope.terminalOutput.splice(0, 1);
            }
        }
    }
    
    function ouputStayAtBottom(){
        /// function to make the output panel to stay at the bottom
        /// when new output is appended
        terminalArea.scrollTop = terminalArea.scrollHeight;
    }
    
    /*
     *  Run, load all datasets
     */
    $scope.datasets = [];
    Compiler.getDatasets().success(function(data) {
        $scope.datasets = data;

    }).error(function() {
        $scope.setAlert(2, "Can't load datasets");
    });
 
    /*
     *  Program Output
     */
    $scope.outputTab = 'terminal';

    function checkProgramStatus() {

        Compiler.checkProgramStatus().success(function(data) {
            $scope.runningProgram = data.runningProgram;

            if (data.result) {
                $scope.programPanel = 'running';
                $scope.statusTimeOut = $timeout(checkProgramStatus, 30000);
            } else if($scope.runningProgram) {
                $scope.setAlert(1, "The program finished running.");
                
                $scope.programPanel = 'close';
                $scope.terminalOutput.push('Last Run: program '+$scope.runningProgram.program_name +' completed!');
                $scope.terminalOutput.push("Output status: " + data.terminalStatus);
                
                if (data.terminal.length > 0) {
                    $scope.terminalOutput.push('Output:');
                    for (n in data.terminal) {
                        $scope.terminalOutput.push(data.terminal[n]);
                    }
                }
                
                $scope.downloadJson = data.downloadJson;
                if (data.json) {
                    $scope.jsonOutput = data.json;
                }

                if (!$scope.downloadJson) {
                    $scope.imageOutput = data.images;
                }

                $timeout.cancel($scope.statusTimeOut);
            }
        });
    }
    checkProgramStatus();
    
    $scope.stopRunningProgram = function(){
        $scope.programPanel = 'loading';
        Compiler.stopProcess().success(function(data) {
            if(data.result){
                $scope.programPanel = 'close';
                $scope.setAlert(1, "The program stops running.");
            }else{
                $scope.programPanel = 'running';
                $scope.setAlert(2, "Can't stop the program");
            }

            $timeout.cancel($scope.statusTimeOut);
        }).error(function() {
            $scope.setAlert(2, "Can't stop the program");
        });
    };
    
    $scope.getTerminalOutput = function(){
        Compiler.getTerminalOutput().success(function(data) {
            $scope.terminalOutput.push('');
            $scope.terminalOutput.push('Last Run: program ' + $scope.runningProgram.program_name + ' completed!');
            $scope.terminalOutput.push("Output status: " + data.terminalStatus);

            if (data.terminal.length > 0) {
                $scope.terminalOutput.push('Output:');
                for (n in data.terminal) {
                    $scope.terminalOutput.push(data.terminal[n]);
                }
            }
        });
    };

    $scope.fileFormat = '';
    $scope.gettingFile = false;
    $scope.selectedOutputFile = null;
    $scope.selectedOutputIndex = null;
    $scope.outputFiles = [];
    $scope.MAX_FILE = null;

    $scope.refreshOutputFolder = function() {
        $scope.gettingFile = true;
        Compiler.getOutputFolder().success(function(data) {

            $scope.gettingFile = false;
            $scope.selectedOutputFile = null;
            $scope.selectedOutputIndex = null;
            $scope.MAX_FILE = data.max_file;

            if (data.result) {
                $scope.outputFiles = data.files;
            } else {
                $scope.setAlert(2, "Can't get file in output folder.");
                $scope.outputFiles = [];
            }
        });
    };
    $scope.refreshOutputFolder();

    $scope.emptyOutputFolder = function() {
        $scope.setModalAlert(4, "Do you want to delete all files in output folder ?", function() {
            Compiler.emptyOutputFolder().success(function(data) {
                if (data.result) {
                    $scope.refreshOutputFolder();
                } else {
                    $scope.setAlert(2, "Can't empty output folder.");
                }
            });
        });
    };

    $scope.getOutputFile = function(index) {
        $scope.selectedOutputIndex = index;
        $scope.selectedOutputFile = $scope.outputFiles[index];
    };

    $scope.deleteOutputFile = function() {
        $scope.setModalAlert(4, "Do you want to delete " + $scope.selectedOutputFile.name + " ?", function() {
            Compiler.deleteOutputFile($scope.selectedOutputFile.name).success(function(data) {
                if (data.result) {
                    $scope.refreshOutputFolder();
                } else {
                    $scope.setAlert(2, "Can't delete the file");
                }
            });
        });
    };

    /*
     *  Create alert modal
     */
    $scope.alertModalMessage = '';
    $scope.alertModalShow = 0;

    $scope.setModalAlert = function(type, msg, callback) {
        $scope.alertModalMessage = msg;
        $scope.alertModalShow = type;

        if (type === 4) {
            $scope.alertCallback = callback;
        } else {
            $scope.alertCallback = function() {
            };
        }

        $("#alertModal").modal('show');
    };
}