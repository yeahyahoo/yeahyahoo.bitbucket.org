function SettingController($scope, $state, Setting) {
    $scope.settings = {};
    
    Setting.load().success(function(data) {
        
        $scope.settings = data;
        $scope.settings.construction = JSON.parse($scope.settings.construction);
        $scope.settings.signup = JSON.parse($scope.settings.signup);
        $scope.settings.max_processes = parseInt($scope.settings.max_processes);
        $scope.settings.max_dataset_size = parseInt($scope.settings.max_dataset_size);
        
    }).error(function() {
    });

    $scope.updateSettings = function() {
        Setting.update($scope.settings).success(function(data) {
            if(data.result){
                $scope.setAlert(1, "Updated");
            }else{
                $scope.setAlert(2, "Can't update settings.");
            }
        }).error(function() {
        });
    }
}