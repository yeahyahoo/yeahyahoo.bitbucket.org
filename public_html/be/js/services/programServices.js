'use strict';

angular.module('programServices', ['ngResource']).factory('Program', function($http) {

    var requestUrl = baseUrl + '/index.php/program/';

    return {
        getById: function(id) {
            return $http.get(requestUrl + 'getbyid', {
                params: {
                    id: id
                }
            });
        },
        getProgramDetails: function() {
            return $http.get(requestUrl + 'getprogramdetails');
        },
        getAll: function() {
            return $http.get(requestUrl + 'getall');
        }
    };
});
