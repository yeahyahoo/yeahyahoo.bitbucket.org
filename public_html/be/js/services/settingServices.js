'use strict';

angular.module('settingServices', []).factory('Setting', ['$http',
    function($http) {

        var requestUrl = baseUrl + '/index.php/be/';

        return {
            load: function() {
                return $http({
                    method: 'GET',
                    url: requestUrl + 'loadsettings',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                });
            },
            update: function(settings) {
                var data = $.param(settings);
                return $http({
                    method: 'POST',
                    url: requestUrl + 'updatesettings',
                    data: data,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                });
            },
        };
    }]);
