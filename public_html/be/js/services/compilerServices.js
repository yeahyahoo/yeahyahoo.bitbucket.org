'use strict';
angular.module('compilerServices', ['ngResource']).factory('Compiler', function($http) {

    var requestUrl = baseUrl + '/index.php/compiler/';

    return {
        createFolder: function(type, name, index) {
            var data = $.param({
                type: type,
                name: name,
                index: index
            });
            return $http({
                method: 'POST',
                url: requestUrl + 'createfolder',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },
        renameFolder: function(id, new_name) {
            var data = $.param({
                id: id,
                new_name: new_name
            });
            return $http({
                method: 'POST',
                url: requestUrl + 'renamefolder',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },
        deleteFolder: function(id) {
            var data = $.param({
                id: id
            });

            return $http({
                method: 'POST',
                url: requestUrl + 'deletefolder',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },
        folderMoveUp: function(folderUp, folderDown, index) {
            var data = $.param({
                folderUp: folderUp,
                folderDown: folderDown,
                index: index
            });
            return $http({
                method: 'POST',
                url: requestUrl + 'folderup',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },
        folderMoveDown: function(folderUp, folderDown, index) {
            var data = $.param({
                folderUp: folderUp,
                folderDown: folderDown,
                index: index
            });
            return $http({
                method: 'POST',
                url: requestUrl + 'folderdown',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },
        folderVisibility: function(id, show) {
            var data = $.param({
                id: id,
                show: show
            });

            return $http({
                method: 'POST',
                url: requestUrl + 'foldervisibility',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },
        create: function(program) {
            var data = $.param(program);
            return $http({
                method: 'POST',
                url: requestUrl + 'new',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },
        loadPrograms: function() {
            return $http({
                method: 'GET',
                url: requestUrl + 'loadprograms',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },
        open: function(id) {
            var data = $.param({
                id: id
            });
            return $http({
                method: 'POST',
                url: requestUrl + 'open',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },
        save: function(program, content) {
            var data = $.param({
                program: program,
                content: content
            });
            return $http({
                method: 'POST',
                url: requestUrl + 'save',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },
        compile: function(program, content) {
            var data = $.param({
                program: program,
                content: content
            });
            return $http({
                method: 'POST',
                url: requestUrl + 'compile',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },
        run: function(program, run_type, dataset_id, data_path, args, content) {
            var data = $.param({
                program: program,
                run_type: run_type,
                dataset_id : dataset_id,
                data_path: data_path,
                args: args,
                content: content
            });
            return $http({
                method: 'POST',
                url: requestUrl + 'run',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },
        rename: function(id, newName) {
            var data = $.param({
                id: id,
                name: newName
            });

            return $http({
                method: 'POST',
                url: requestUrl + 'rename',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },
        delete: function(program) {
            var data = $.param(program);
            return $http({
                method: 'POST',
                url: requestUrl + 'delete',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },
        loadTheme: function() {
            return $http({
                method: 'GET',
                url: requestUrl + 'loadtheme',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },
        updateTheme: function(id, theme) {
            var data = $.param({
                id: id,
                theme: theme,
            });
            return $http({
                method: 'POST',
                url: requestUrl + 'updatetheme',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },
        getDatasets: function() {
            return $http({
                method: 'GET',
                url: requestUrl + 'getdatasets',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },
        stopProcess: function() {
            return $http({
                method: 'POST',
                url: requestUrl + 'stopprocess',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },
        checkProgramStatus: function() {
            return $http({
                method: 'GET',
                url: requestUrl + 'checkprogramstatus',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },
        getTerminalOutput: function() {
            return $http({
                method: 'POST',
                url: requestUrl + 'getterminaloutput',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },
        getOutputFolder: function() {
            return $http({
                method: 'GET',
                url: requestUrl + 'getoutputfolder',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },
        emptyOutputFolder: function() {
            return $http({
                method: 'POST',
                url: requestUrl + 'emptyoutputfolder',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },
        deleteOutputFile: function(name) {
            var data = $.param({
                name: name
            });
            return $http({
                method: 'POST',
                url: requestUrl + 'deleteoutputfile',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },
    };
});
