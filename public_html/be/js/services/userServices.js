'use strict';

angular.module('userServices', []).factory('User', ['$http',
    function($http) {

        var requestUrl = baseUrl + '/index.php/user/';

        return {
            getAllUsers: function() {
                return $http({
                    method: 'POST',
                    url: requestUrl + 'getallusers',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                });
            }
        };
    }]);
