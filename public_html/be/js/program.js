function ProgramController ($scope, $state, Program, Process) {
    
    $scope.subNav = 'programs';
    $scope.classifiers = []; /// classifier program
    $scope.datasets = [];
    $scope.totalClassifier = 0;
    $scope.totalDataset = 0;
    
    $scope.selectedProgram = null;
    $scope.selectedProcesses = [];
    if ($state.params.id != undefined) {  
        $scope.subNav = 'processlist';
        Program.getById($state.params.id).success(function(data) {
            $scope.selectedProgram = data.program;
            $scope.selectedProcesses = data.processes;

        }).error(function() {
            $scope.setAlert(2, "Can't load program");
        });
    }else{
        Program.getProgramDetails().success(function(data) {
            $scope.classifierFolders = data.classifierFolders;
            $scope.datasetFolders = data.datasetFolders;
            $scope.totalDataset = data.totalDataset;
            $scope.totalClassifier = data.totalClassifier;
            
        }).error(function() {
            $scope.setAlert(2, "Can't load programs");
        });
    }

    $scope.activeProcessIndex = null;
    
    $scope.assignActiveHover = function(index){
        $scope.activeProcessIndex = index;
    };

    $scope.stopProcess = function(process) {
        $scope.setAlert(4, "Do you want to stop this process (" + process.formated_start_time + ") ?", function() {

            Process.stopProcess(process.id).success(function(data) {
                if (data.result) {
                    $scope.setAlert(1, "The process has been canceled.");
                    $scope.activeProcesses.splice($scope.activeProcessIndex, 1);
                } else {
                    $scope.setAlert(2, "Can't stop this process.");
                }
            }).error(function() {
                $scope.setAlert(2, "Can't stop this process.");
            });
        });
    };

}