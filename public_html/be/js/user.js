function UserController($scope, $state, User) {

    $scope.users = [];
    
    User.getAllUsers().success(function(data) {
        $scope.users = data;

    }).error(function() {
        $scope.setAlert(2, "Can't load users");
    });

    $scope.activeProcessIndex = null;
    
    $scope.assignActiveHover = function(index){
        $scope.activeProcessIndex = index;
    }
}