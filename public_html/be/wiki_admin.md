## Introduction ##

The only important feature on back-end is the program editor. We are going to learn how to use it to write programs for the application

There are two types of programs: classifier and dataset. A classifier program is understood as an 'algorithm' in the front application. A dataset program is called as 'modifier'. The editor supports two languages: C and Python 2.7

We will write program as normal C or Python. When the program runs, it receives the following arguments:

###Classifier program:###

argv[0] : full path to the program

argv[1] : a path to user folder. Typically it would look like this:

    /var/www/ml_project/user_data/52339f0878c9e7854ce5c899/

The hashed number 52339f0878c9e7854ce5c899 is user id. Inside this folder, we have two folders: dataset and out. Dataset will store all csv files. 'out' folder stores output files ( json files and images ).

argv[2] : process id , a hashed number like **52339f0878c9e7854ce5c899**

argv[3] : classifier id . Its purpose is to used as a name to an output file. It's unique for a classifier. Therefore we can store data in output file with this id as a name to share with other processes of this classifier.

argv[4] : '0' or '1' . The classifier program receives '0' as  4th argument when a new classifier is created. The program receives '1' when we continue to run the classifier.

argv[5] : full path to dataset . It looks like:

     /var/www/ml_project/user_data/52339f0878c9e7854ce5c899/datasets/5233c98278c9e7a04a4f1402.csv

Dataset is store in file system, not in database. A program can read and write directly. Everytime we run a program with a dataset, we marked the dataset as being processed. ( format csv at the end )

argv[6....] : user defined arguments

###Dataset program arguments:###

argv[1] : a path to user folder. Typically it would look like this:

    /var/www/ml_project/user_data/52339f0878c9e7854ce5c899/

The hashed number 52339f0878c9e7854ce5c899 is user id. Inside this folder, we have two folders: dataset and out. Dataset will store all csv files. 'out' folder stores output files ( json files and images ).

argv[2] : process id , a hashed number like **52339f0878c9e7854ce5c899**

argv[3] : full path to dataset . It looks like:

     /var/www/ml_project/user_data/52339f0878c9e7854ce5c899/datasets/5233c98278c9e7a04a4f1402.csv

Dataset is store in file system, not in database. A program can read and write directly. Everytime we run a program with a dataset, we marked the dataset as being processed. ( format csv at the end )

argv[4....] : user defined arguments


## Classifier program ##

We write a classifier program as usually. The only we care it what to output. We can have printf statement in c , or print function in python to show any desired output.

The application also receives a json file to display output. The task of administrator is to write this json file if we want to show the following formats:

- Charts: scatter plot, histogram , line chart
- Image plot: using GNUplot or matplotlib
- Table
- Decision Tree

let's do an example:
- Open the following proram , type : Classifier. Folder: Test , Program : tree.python


- Click 'Run' button. After a short time, we see a form. Select any dataset ( only dataset which is not processed is visible ) and hit 'Run'. After 30 seconds, the terminal output at the bottom will say that 'Program completed !'. This program is simple so it's finished instantly. There is a script to auto check every 30 seconds if the program is done or not.

```

#!/usr/bin/python
import sys
import json

print 'Number of arguments:', len(sys.argv), 'arguments.'
print 'Argument List:', str(sys.argv)

data = [
   {
	'type':'tree',
	'name': 'Decision Tree',
	'data':{
			'name': 'root',
			'children':[
				{
					'name': 'left node',
					'linkLabel': 'yes',
					'children': [ ]
				},
				{
					'name': 'right node',
					'linkLabel': 'no',
					'children': [ ]
				}
			]
	   }
   }
];


with open(sys.argv[1]+sys.argv[2]+'.json', 'w') as outfile:
  json.dump(data, outfile, indent=4)

```

This program will output a json file with a tree format. The front application will read this format and display it. We have to write a format like that and save it to
    sys.argv[1]+sys.argv[2]+'.json'


Here is what we will see in front-end application:

![Tree](guide/tree.png)

To display more mature plot, we will use GNUplot or matplotlib (python ).

Example:
- Open the following program, type: classifier, folder: Test , program: Scatter plot (GNUplot).c
- Click 'Compile & run'. Because this is a c program, we have to compile first.

```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <jansson.h>

int main(int argc, char **argv){

   json_t *json_outputs = json_array();
   json_t *image_plot = json_object();
   json_t *image_plot_2 = json_object();

   json_array_append(json_outputs, image_plot);
   json_array_append(json_outputs, image_plot_2);
   .....

   .....
   fprintf(pipe, out_str_2);
   fprintf(pipe, "plot sin(x)\n");

   printf("%s", out_str_2);

   fprintf(pipe, "quit");
   close(pipe);

   return 0;
}

```

   Click 'Output folder' tab at the bottom. We will see following json.

```

[
    {
        "data":{
            "name":"51c4cbbe00c857073a0000001.png"
        },
        "type":"image_plot",
        "name":"Sample Scatter Plot"
    },{
        "data":{
            "name":"51c4cbbe00c857073a0000002.png"
        },
        "type":"image_plot",
        "name":"Sample Scatter Plot 2"
    }
]

```

![Plots](guide/plots.png)

The C program use Jansson library to output json file. We can use any library installed on server as long as we save the file to  argv[1] + argv[2] + '.json' . Here is the above code that concatenate strings.


```

   char json_str[100];
   sprintf(json_str,"%s%s%s",argv[1],argv[2],".json");

```

Note that string length is 100. The path, argv[1]+argv[2], is actually longer than 50 characters. It's safe to use 100 characters.
The application is able to display multiple outputs. We can a output type 'table', give it a name, and another output show a plot.
It's very important to have a sample run using editor first to check for a valid format before running the program in front application.
The last section show formats of all output types.

## Dataset program ##
This program is easier to write then program. We don't have to care output. Its sole purpose is to read the dataset path in argv[3], then modify it in any way we want , then save it to the same path argv[3] again. Since the application only support dataset < 500 mb. The approach may have a problem if we try to process a large dataset since we have to read a whole dataset into memory.

Sample program: type 'dataset', folder 'Basic', name 'Shuffle Data.python'

```

#!/usr/bin/python
import sys
import os
os.environ['MPLCONFIGDIR']="/tmp"
import random

file_name = sys.argv[3]

with open(file_name, "r+") as f:
    dataset = f.readlines()
    random.shuffle(dataset)
    f.seek(0)
    f.writelines(dataset)
    f.close()

```

## Editor ##

Here are some features of the editor:

- Description: We we will a description of program. It will show in the font application. We could write any important detail that we need to let user know.

- Arguments: if we would like to add extra arguments to the program. The argument is received as a string. We have to convert it to float or int upon receiving it. There is no method to prevent user to input any wrong value.

- Output: there are 3 tabs: terminal , json file and images. Terminal tab show any stdout or stderr that we receive during running application. Json file show a json file that we save. Again, its path is always argv[1] + argv[2] + '.json' . Images tab show image plots that we defined in json. We have to define an image plot in json file first before seeing the plot.

  If the program runs for a long time, we can close browser. After we open the editor again, we will see an indicator that shows if the program is still running or not. If it's done. We can click 'Last output' button in output tab to view output from last running.

## Json Formats ##

1. Image plot:
   To display a plot, we have to define it in json first, name it argv[2] + '.png ' . Then save the image to
argv[1] + argv[2] + '.png' . The above c program is a good reference.
   To display more plots, we name it argv[2] + '1.png' for first image, argv[2] + '2.png' for second image .etc. Then save the files with corresponding names.
   The above program is a good preference.

Format:

```
[
    {
        "type":"image_plot",
        "name":"Sample Scatter Plot 2",
        "data":{
            "name":"51c4cbbe00c857073a0000002.png"
        }
    }
]

```

2. Scatter plot


3. Histogram


4. Line chart


5. Table

```

[
   {
	type: 'table',
        name: 'Confusion matrix',
        data: {
	   headers: ['index','x','y'],
           rows:[
			{ 1, 2.2, 3.1 },
			{ 2, 3.1, 4.2 } ,
			{ 3, 2.3, 3.4}
		]
	}
    }
]

```
6. Decision tree

```

[
    {
            type:'tree',
            name: 'Decision Tree',
            data:{
                name: 'root',
                children:[
                        {
                                name: 'left node',
                                children: [ ]
                        },
                        {
                                name: 'right node',
                                children: [ ]
                        }
                ]
            }
    }
]

```
