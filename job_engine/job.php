<?php
    /// arguments
    /*
     * 0: this file, 1: command_line , 2:process_id, 3:dataset_id
     */
    include('../application/models/Processes.php');
    include('../application/models/Datasets.php');
    
    $output = array();
    $programStatus = 0;
    exec($argv[1], $output, $programStatus);
    
    $status = 'done';
    if(intval($programStatus)>0){
        $status = 'error';
    }
    
    //// save to process 
    $process_model = new Model_Processes();
    $process_model->update(array('_id'=>new MongoId($argv[2])), array('$set' => array(
        "command_line" => $argv[1],
        "output" => $output,
        "status" => $status,
        "end_time" => new MongoDate()
    )));
    
    $dataset_model = new Model_Datasets();
    $dataset_model->update(array('_id'=>new MongoId($argv[3])), array('$set' => array(
        "processing" => false,
    )));
?>