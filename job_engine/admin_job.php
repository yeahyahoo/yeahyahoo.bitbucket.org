<?php
    /// arguments
    /*
     * 0: this file, 1: command_line , 2:admin_id, 3:dataset_id
     */
    include('/home/tlam/application/models/Admin.php');
    include('/home/tlam/application/models/Datasets.php');
    
    /// run program
    $terminalOutput = array();
    $terminalStatus = 0;
    
    exec($argv[1], $terminalOutput, $terminalStatus);
    
    $status = 'done';
    if(intval($terminalStatus)>0){
        $status = 'error';
    }
    
    //// save to process 
    $admin_model = new Model_Admin();
    $admin_model->update(array('_id'=>new MongoId($argv[2])), array('$set' => array(
        "program_output" => $terminalOutput,
        "program_status" => $status
    )));
    
    $dataset_model = new Model_Datasets();
    $dataset_model->update(array('_id'=>new MongoId($argv[3])), array('$set' => array(
        "processing" => false,
    )));
?>